#include "PoissonAGH/Poisson2D.hpp"
#include "gtest/gtest.h"

extern "C"
{
#include "PoissonAGH/Poisson2D_C.h"
}

using namespace testing;
using namespace PoissonAGH;

TEST(Poisson2DAPITestSuite, createUsingSquareStepConstructor)
{
    Poisson2D_Ptr solver = P2D_new_Poisson2D_squareStep(0, 1, 0.1);

    EXPECT_FALSE(P2D_isSolved(solver));
    EXPECT_EQ(0, P2D_getXmin(solver));
    EXPECT_EQ(1, P2D_getXmax(solver));
    EXPECT_EQ(0, P2D_getYmin(solver));
    EXPECT_EQ(1, P2D_getYmax(solver));
    EXPECT_EQ(0.1, P2D_getStep(solver));
    EXPECT_EQ(9u, P2D_getNx(solver));
    EXPECT_EQ(9u, P2D_getNy(solver));

    P2D_delete_Poisson2D(solver);
}

TEST(Poisson2DAPITestSuite, createUsingSquareSizeConstructor)
{
    Poisson2D_Ptr solver = P2D_new_Poisson2D_squareSize(0, 1, 9u);

    EXPECT_FALSE(P2D_isSolved(solver));
    EXPECT_EQ(0, P2D_getXmin(solver));
    EXPECT_EQ(1, P2D_getXmax(solver));
    EXPECT_EQ(0, P2D_getYmin(solver));
    EXPECT_EQ(1, P2D_getYmax(solver));
    EXPECT_EQ(0.1, P2D_getStep(solver));
    EXPECT_EQ(9u, P2D_getNx(solver));
    EXPECT_EQ(9u, P2D_getNy(solver));

    P2D_delete_Poisson2D(solver);
}

TEST(Poisson2DAPITestSuite, createUsingRectangleStepConstructor)
{
    Poisson2D_Ptr solver = P2D_new_Poisson2D_rectangleStep(0, 1, -1, 2, 0.1);

    EXPECT_FALSE(P2D_isSolved(solver));
    EXPECT_EQ(0, P2D_getXmin(solver));
    EXPECT_EQ(1, P2D_getXmax(solver));
    EXPECT_EQ(-1, P2D_getYmin(solver));
    EXPECT_EQ(2, P2D_getYmax(solver));
    EXPECT_EQ(0.1, P2D_getStep(solver));
    EXPECT_EQ(9u, P2D_getNx(solver));
    EXPECT_EQ(29u, P2D_getNy(solver));

    P2D_delete_Poisson2D(solver);
}

TEST(Poisson2DAPITestSuite, createUsingMatrixConstructor)
{
    size_t rows = 6;
    size_t cols = 5;
    double** matrix = (double**) malloc(rows * sizeof(double*));
    for(size_t i = 0; i < rows; ++i)
    {
        matrix[i] = (double*) malloc(cols * sizeof(double));
        matrix[i][0] = i*2+1;
        matrix[i][1] = i*3+2;
        matrix[i][2] = i*4+3;
        matrix[i][3] = i*5+1;
        matrix[i][4] = i*6+2;
    }

    Poisson2D_Ptr solver = P2D_new_Poisson2D_matrix(matrix, rows, cols, 0.1);

    EXPECT_FALSE(P2D_isSolved(solver));
    EXPECT_EQ(0.1, P2D_getStep(solver));
    EXPECT_EQ(rows-2, P2D_getNx(solver));
    EXPECT_EQ(cols-2, P2D_getNy(solver));

    const double* data = P2D_getRhoMatrix(solver);

    for(size_t i = 0; i < rows; ++i)
    {
        for(size_t j = 0; j < cols; ++j)
        {
            EXPECT_EQ(matrix[i][j], P2D_getValueAt(data, cols, i, j));
        }
    }

    P2D_delete_Poisson2D(solver);
}

TEST(Poisson2DAPITestSuite, createUsingArrayConstructor)
{
    size_t rows = 6;
    size_t cols = 5;

    double array[30] = {0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9};

    Poisson2D_Ptr solver = P2D_new_Poisson2D_array(array, rows, cols, 0.1);

    EXPECT_FALSE(P2D_isSolved(solver));
    EXPECT_EQ(0.1, P2D_getStep(solver));
    EXPECT_EQ(rows-2, P2D_getNx(solver));
    EXPECT_EQ(cols-2, P2D_getNy(solver));

    const double* data = P2D_getRhoMatrix(solver);

    for(size_t i = 0; i < rows*cols; ++i)
    {
        EXPECT_EQ(data[i], array[i]);
    }

    P2D_delete_Poisson2D(solver);
}

TEST(Poisson2DAPITestSuite, shallSetEpsilonC)
{
    Poisson2D_Ptr solver = P2D_new_Poisson2D_squareStep(0.5, 1, 0.1);

    P2D_setEpsilonC(solver, 3.1415);
    EXPECT_EQ(3.1415, P2D_getEpsilonC(solver));

    P2D_delete_Poisson2D(solver);
}

TEST(Poisson2DAPITestSuite, shallSetEpsilonMatrix)
{
    Poisson2D_Ptr solver = P2D_new_Poisson2D_squareSize(1, 2, 1);

    double epsilon[9] = {0,-1,-2,-3,-4,-5,-6,-7,-8};

    P2D_setEpsilonMatrix(solver, epsilon);

    const double* data = P2D_getEpsilonMatrix(solver);

    for(int i = 0; i < 9; ++i)
    {
        EXPECT_EQ(data[i], -i);
    }

    P2D_delete_Poisson2D(solver);
}

TEST(Poisson2DAPITestSuite, shallSetBoundaryConditions)
{
    size_t rows = 6;
    size_t cols = 5;

    double array[30] = {0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9};

    double west[6] = {111,11,12,13,14,222};
    double east[6] = {333,21,22,23,24,444};
    double north[5] = {222,31,32,33,444};
    double south[5] = {111,41,42,43,333};

    Poisson2D_Ptr solver = P2D_new_Poisson2D_array(array, rows, cols, 0.1);

    P2D_setCondition(solver, west, 6, DIRICHLET_WEST);
    P2D_setCondition(solver, east, 6, DIRICHLET_EAST);
    P2D_setCondition(solver, north, 5, DIRICHLET_NORTH);
    P2D_setCondition(solver, south, 5, DIRICHLET_SOUTH);

    const double* phi = P2D_getPhiMatrix(solver);

    for(size_t i = 0; i < rows; ++i)
    {
        EXPECT_EQ(west[i], P2D_getValueAt(phi, cols, i, 0));
        EXPECT_EQ(east[i], P2D_getValueAt(phi, cols, i, 4));
    }

    for(size_t j = 0; j < cols; ++j)
    {
        EXPECT_EQ(north[j], P2D_getValueAt(phi, cols, 5, j));
        EXPECT_EQ(south[j], P2D_getValueAt(phi, cols, 0, j));
    }

    EXPECT_EQ((int)DIRICHLET_NORTH, P2D_getNorthCondition(solver));
    EXPECT_EQ(DIRICHLET_WEST, P2D_getWestCondition(solver));
    EXPECT_EQ(DIRICHLET_EAST, P2D_getEastCondition(solver));
    EXPECT_EQ(DIRICHLET_SOUTH, P2D_getSouthCondition(solver));

    P2D_setCondition(solver, west, 6, NEUMANN_WEST);
    P2D_setCondition(solver, east, 6, NEUMANN_EAST);
    P2D_setCondition(solver, north, 5, NEUMANN_NORTH);
    P2D_setCondition(solver, south, 5, NEUMANN_SOUTH);

    EXPECT_EQ(NEUMANN_NORTH, P2D_getNorthCondition(solver));
    EXPECT_EQ(NEUMANN_WEST, P2D_getWestCondition(solver));
    EXPECT_EQ(NEUMANN_EAST, P2D_getEastCondition(solver));
    EXPECT_EQ(NEUMANN_SOUTH, P2D_getSouthCondition(solver));

    P2D_delete_Poisson2D(solver);
}

double density(double x, double y)
{
    return 2*( (1-6*x*x)*y*y*(1-y*y) + (1-6*y*y)*x*x*(1-x*x) );
};

double solution(double x, double y)
{
    return (x*x-x*x*x*x) * (y*y*y*y - y*y);
};

TEST(Poisson2DAPITestSuite, shallSolveProblem)
{
    using namespace std;
    double xmin = 0;
    double xmax = 1;
    size_t nx = 63;
    double h = (xmax - xmin) / static_cast<double>(nx+1);

    double rho[(nx+2)*(nx+2)];

    for(size_t i = 0; i < nx+2; ++i)
    {
       for(size_t j = 0; j < nx+2; ++j)
       {
           P2D_setValueAt(rho, density(xmin + i*h, xmin + j*h), nx+2, i, j);
       }
    }

    Poisson2D_Ptr solver = P2D_new_Poisson2D_array(rho, nx+2, nx+2, h);

    P2D_setSolverSteps(solver, 2);
    P2D_setMaxMultigridCalls(solver, 3);
    P2D_setResidualDelta(solver, 1);
    P2D_setMultigridType(solver, FULL_MULTIGRID);
    P2D_setEpsilonC(solver, 1);
    P2D_solve(solver);

    const double* phi = P2D_getPhiMatrix(solver);

    for(size_t i = 0; i < nx+2; ++i)
    {
        for(size_t j = 0; j < nx+2; ++j)
        {
            EXPECT_NEAR(solution(xmin+i*h, xmin+j*h), P2D_getValueAt(phi, nx+2, i, j), 1e-4);
        }
    }

    P2D_delete_Poisson2D(solver);
}
