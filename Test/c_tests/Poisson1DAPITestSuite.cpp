#include "PoissonAGH/Poisson1D.hpp"
#include "gtest/gtest.h"

extern "C"
{
#include "PoissonAGH/Poisson1D_C.h"
}

using namespace testing;
using PoissonAGH::Poisson1D;

TEST(Poisson1DAPITestSuite, createUsingStepConstructor)
{
    Poisson1D_Ptr solver = P1D_new_Poisson1D_step(0.5, 1, 0.1);

    EXPECT_FALSE(P1D_isSolved(solver));
    EXPECT_EQ(0.5, P1D_getXmin(solver));
    EXPECT_EQ(1, P1D_getXmax(solver));
    EXPECT_EQ(0.1, P1D_getStep(solver));
    EXPECT_EQ(5u, P1D_getNx(solver));

    P1D_delete_Poisson1D(solver);
}

TEST(Poisson1DAPITestSuite, createUsingSizeConstructor)
{
    Poisson1D_Ptr solver = P1D_new_Poisson1D_size(0.5, 1, 10);

    EXPECT_FALSE(P1D_isSolved(solver));
    EXPECT_EQ(0.5, P1D_getXmin(solver));
    EXPECT_EQ(1, P1D_getXmax(solver));
    EXPECT_EQ(0.05, P1D_getStep(solver));
    EXPECT_EQ(10u, P1D_getNx(solver));

    P1D_delete_Poisson1D(solver);
}

TEST(Poisson1DAPITestSuite, createUsingArrayConstructor)
{
    double arr[5] = {0.1, 0.2, 0.3, 0.4, 0.5};
    Poisson1D_Ptr solver = P1D_new_Poisson1D_array(arr, 5, 0.1);

    EXPECT_EQ(0.1, P1D_getStep(solver));
    EXPECT_EQ(5u, P1D_getNx(solver));

    const double* rho = P1D_getRhoArray(solver);

    for(int i = 0; i < 5; ++i)
    {
        EXPECT_EQ(arr[i], rho[i]);
    }

    P1D_delete_Poisson1D(solver);
}

TEST(Poisson1DAPITestSuite, shallSetEpsilonC)
{
    Poisson1D_Ptr solver = P1D_new_Poisson1D_step(0.5, 1, 0.1);

    P1D_setEpsilonC(solver, 3.1415);
    EXPECT_EQ(3.1415, P1D_getEpsilonC(solver));

    P1D_delete_Poisson1D(solver);
}

TEST(Poisson1DAPITestSuite, shallSetLaplaceC)
{
    Poisson1D_Ptr solver = P1D_new_Poisson1D_step(0.5, 1, 0.1);

    P1D_setLaplaceC(solver, 0.987);
    EXPECT_EQ(0.987, P1D_getLaplaceC(solver));

    P1D_delete_Poisson1D(solver);
}

TEST(Poisson1DAPITestSuite, shallSetEpsilonArray)
{
    Poisson1D_Ptr solver = P1D_new_Poisson1D_step(0.5, 1, 0.1);

    double arr[5] = {0.2, 0.4, 0.6, 0.9, 1.2};

    P1D_setEpsilonArray(solver, arr);
    const double* epsilon = P1D_getEpsilonArray(solver);

    for(int i = 0; i < 5; ++i)
    {
        EXPECT_EQ(epsilon[i], arr[i]);
    }

    P1D_delete_Poisson1D(solver);
}

TEST(Poisson1DAPITestSuite, shallSetDirichletConditions)
{
    Poisson1D_Ptr solver = P1D_new_Poisson1D_step(0.5, 1, 0.1);

    P1D_setDirichletConditions(solver, 2.5, 5.8);

    EXPECT_EQ(2.5, P1D_getLeftDirichlet(solver));
    EXPECT_EQ(5.8, P1D_getRightDirichlet(solver));

    P1D_delete_Poisson1D(solver);
}

TEST(Poisson1DAPITestSuite, shallSetNeumannConditions)
{
    Poisson1D_Ptr solver = P1D_new_Poisson1D_step(0.5, 1, 0.1);

    P1D_setNeumannConditions(solver, 2.5, 5.8);

    EXPECT_EQ(2.5, P1D_getLeftNeumann(solver));
    EXPECT_EQ(5.8, P1D_getRightNeumann(solver));

    P1D_delete_Poisson1D(solver);
}

TEST(Poisson1DAPITestSuite, shallSetEveryConditionType)
{
    Poisson1D_Ptr solver = P1D_new_Poisson1D_step(0.5, 1, 0.1);

    P1D_setConditionsType(solver, DIRICHLET_DIRICHLET);
    EXPECT_EQ(DIRICHLET_DIRICHLET, P1D_getConditionType(solver));

    P1D_setConditionsType(solver, DIRICHLET_NEUMANN);
    EXPECT_EQ(DIRICHLET_NEUMANN, P1D_getConditionType(solver));

    P1D_setConditionsType(solver, NEUMANN_DIRICHLET);
    EXPECT_EQ(NEUMANN_DIRICHLET, P1D_getConditionType(solver));

    P1D_setConditionsType(solver, PERIODIC);
    EXPECT_EQ(PERIODIC, P1D_getConditionType(solver));

    P1D_delete_Poisson1D(solver);
}

namespace
{
double epsFunction(double x)
{
    return 2.+cos(M_PI*x);
}
double rhoFunction(double x)
{
    return -50.*exp(-50.*(x-0.5)*(x-0.5));
}

}

TEST(Poisson1DAPITestSuite, shallSolveProblem)
{
    double tol = 1e-3;
    const int N = 99;
    double dx = 1./(N+1);
    std::vector<double> sol =
            { 0.0333399, 0.0666907, 0.100064, 0.133469, 0.166919, 0.200424,
              0.233994, 0.267642, 0.301379, 0.335215, 0.369162, 0.403231, 0.437434,
              0.471781, 0.506284, 0.540954, 0.575802, 0.610837, 0.64607, 0.681509,
              0.717161, 0.753033, 0.78913, 0.825452, 0.861999, 0.898765, 0.93574,
              0.97291, 1.01025, 1.04774, 1.08533, 1.12299, 1.16064, 1.19823,
              1.23568, 1.27289, 1.30976, 1.34617, 1.382, 1.41711, 1.45135, 1.48458,
              1.51664, 1.54737, 1.57661, 1.60421, 1.63004, 1.65395, 1.67584,
              1.69561, 1.71317, 1.72847, 1.74147, 1.75215, 1.76054, 1.76665,
              1.77055, 1.7723, 1.77199, 1.76972, 1.7656, 1.75975, 1.75229, 1.74334,
              1.73302, 1.72146, 1.70876, 1.69502, 1.68036, 1.66485, 1.64857,
              1.63159, 1.61398, 1.59579, 1.57707, 1.55784, 1.53816, 1.51804,
              1.4975, 1.47658, 1.45528, 1.43362, 1.41162, 1.38929, 1.36664,
              1.34368, 1.32044, 1.29693, 1.27315, 1.24914, 1.2249, 1.20045,
              1.17581, 1.15101, 1.12607, 1.101, 1.07584, 1.0506, 1.02531 } ;

    double rho[N];
    double eps[N];
    for(int i = 0; i < N; ++i)
    {
        eps[i] = epsFunction((i+1)*dx);
        rho[i] = rhoFunction((i+1)*dx);
    }

    Poisson1D_Ptr solver = P1D_new_Poisson1D_array(rho, N, dx);
    P1D_setDirichletConditions(solver, 0., 1.);
    P1D_setEpsilonArray(solver, eps);
    P1D_solve(solver);

    const double* phi = P1D_getPhiArray(solver);

    for(int i = 0; i < N; ++i)
    {
        EXPECT_NEAR(phi[i], sol[i], tol);
    }

}

