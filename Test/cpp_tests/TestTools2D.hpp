#ifndef POISSONAGH_TESTTOOLS2D_HPP
#define POISSONAGH_TESTTOOLS2D_HPP

#include "gtest/gtest.h"
#include "PoissonAGH/Solver/SolverStrategy2D.hpp"
#include "PoissonAGH/Poisson2D.hpp"

namespace PoissonAGH
{
using namespace testing;
using namespace std;

template<typename FunctionSet2D>
void applyConditionNorth(Matrix2D& matrix, ConditionType2D condition, double xmax, double ymin, double h)
{
    for(size_t i = 0; i < matrix.cols(); ++i)
    {
        condition == ConditionType2D::DirichletNorth ?
                matrix(matrix.rows()-1, i) = FunctionSet2D::solution(xmax, ymin + i*h) :
                matrix(matrix.rows()-1, i) = FunctionSet2D::dervX(xmax-h, ymin + i*h);
    }
}

template<typename FunctionSet2D>
void applyConditionSouth(Matrix2D& matrix, ConditionType2D condition, double xmin, double ymin, double h)
{
    for(size_t i = 0; i < matrix.cols(); ++i)
    {
        condition == ConditionType2D::DirichletSouth ?
                matrix(0, i) = FunctionSet2D::solution(xmin, ymin + i*h) :
                matrix(0, i) = FunctionSet2D::dervX(xmin+h, ymin + i*h);
    }
}

template<typename FunctionSet2D>
void applyConditionWest(Matrix2D& matrix, ConditionType2D condition, double xmin, double ymin, double h)
{
    for(size_t i = 0; i < matrix.rows(); ++i)
    {
        condition == ConditionType2D::DirichletWest ?
                matrix(i, 0) = FunctionSet2D::solution(xmin + i*h, ymin) :
                matrix(i, 0) = FunctionSet2D::dervY(xmin + i*h, ymin+h);
    }
}

template<typename FunctionSet2D>
void applyConditionEast(Matrix2D& matrix, ConditionType2D condition, double xmin, double ymax, double h)
{
    for(size_t i = 0; i < matrix.rows(); ++i)
    {
        condition == ConditionType2D::DirichletEast ?
                matrix(i, matrix.cols()-1) = FunctionSet2D::solution(xmin + i*h, ymax) :
                matrix(i, matrix.cols()-1) = FunctionSet2D::dervY(xmin + i*h, ymax-h);
    }
}

template<typename FunctionSet2D>
void checkResult(const Matrix2D& phi, double xmin, double ymin, double h, double error)
{
    double absError = 0;
    double norm = 0;
    for(size_t i = 1; i < phi.rows()-1; ++i)
    {
        for(size_t j = 1; j < phi.cols()-1; ++j)
        {
            EXPECT_NEAR(phi(i, j), FunctionSet2D::solution(xmin + i * h, ymin + j * h), error);
            absError += std::fabs(phi(i, j) - FunctionSet2D::solution(xmin + i * h, ymin + j * h));
            norm += pow(phi(i, j) - FunctionSet2D::solution(xmin + i * h, ymin + j * h), 2);
        }
    }
    cout << "error: " << absError << endl;
    cout << "norm: " << sqrt(norm*h*h) << endl;
}


template<typename FunctionSet2D>
void applyConditionNorth(Array1D& array, ConditionType2D condition, double xmax, double ymin, double h)
{
    for(size_t i = 0; i < array.size(); ++i)
    {
        condition == ConditionType2D::DirichletNorth ?
                array[i] = FunctionSet2D::solution(xmax, ymin + i*h) :
                array[i] = FunctionSet2D::dervX(xmax-h, ymin + i*h);
    }
}

template<typename FunctionSet2D>
void applyConditionSouth(Array1D& array, ConditionType2D condition, double xmin, double ymin, double h)
{
    for(size_t i = 0; i < array.size(); ++i)
    {
        condition == ConditionType2D::DirichletSouth ?
                array[i] = FunctionSet2D::solution(xmin, ymin + i*h) :
                array[i] = FunctionSet2D::dervX(xmin+h, ymin + i*h);
    }
}

template<typename FunctionSet2D>
void applyConditionWest(Array1D& array, ConditionType2D condition, double xmin, double ymin, double h)
{
    for(size_t i = 0; i < array.size(); ++i)
    {
        condition == ConditionType2D::DirichletWest ?
                array[i] = FunctionSet2D::solution(xmin + i*h, ymin) :
                array[i] = FunctionSet2D::dervY(xmin + i*h, ymin+h);
    }
}

template<typename FunctionSet2D>
void applyConditionEast(Array1D& array, ConditionType2D condition, double xmin, double ymax, double h)
{
    for(size_t i = 0; i < array.size(); ++i)
    {
        condition == ConditionType2D::DirichletEast ?
                array[i] = FunctionSet2D::solution(xmin + i*h, ymax) :
                array[i] = FunctionSet2D::dervY(xmin + i*h, ymax-h);
    }
}

template<typename FunctionSet2D>
void createBoundaryAndApplyCondition(Poisson2D& problem, ConditionType2D condition, size_t size,
                                     double xmin, double ymin, double xmax, double ymax, double h)
{
    Array1D boundary(size + 2);
    switch(condition)
    {
        case ConditionType2D::DirichletNorth: // [fallthrough]
        case ConditionType2D::NeumannNorth:
        {
            applyConditionNorth<FunctionSet2D>(boundary, condition, xmax, ymin, h);
            break;
        }
        case ConditionType2D::DirichletSouth: // [fallthrough
        case ConditionType2D::NeumannSouth:
        {
            applyConditionSouth<FunctionSet2D>(boundary, condition, xmin, ymin, h);
            break;
        }
        case ConditionType2D::DirichletWest: // [fallthrough
        case ConditionType2D::NeumannWest:
        {
            applyConditionWest<FunctionSet2D>(boundary, condition, xmin, ymin, h);
            break;
        }
        case ConditionType2D::DirichletEast: // [fallthrough
        case ConditionType2D::NeumannEast:
        {
            applyConditionEast<FunctionSet2D>(boundary, condition, xmin, ymax, h);
            break;
        }
    }
    problem.setCondition(boundary, condition);
}

inline void fillMatrix2DWithFunction(Matrix2D& matrix, Function2D func, double xmin, double ymin, double h)
{
    for(size_t i = 0; i < matrix.rows(); ++i)
    {
        for(size_t j = 0; j < matrix.cols(); ++j)
        {
            matrix(i, j) = func(xmin + i * h, ymin + j * h);
        }
    }
}

inline void checkResidual(Matrix2D& rho, double residuum, double expected)
{
    auto residual = std::accumulate(rho.getData(), rho.getData()+rho.rows()*rho.cols(), 0.);
    cout << residual << endl;
    cout << residuum << endl;
    EXPECT_NEAR(residuum, expected, fabs(expected) / 100);
}

// TODO cpp file + move definitions
inline void assertResultNear(const Poisson2D& problem, Function2D solution, double xmin, double ymin, double tolerance)
{
    for(size_t i = 1; i <= problem.nX(); ++i)
    {
        for(size_t j = 1; j <= problem.nY(); ++j)
        {
            ASSERT_NEAR(problem.phi(i, j), solution(xmin+i*problem.gridStep(), ymin+j*problem.gridStep()), tolerance);
        }
    }
}

inline double calculate2DL2Norm(const Poisson2D& problem, Function2D solution, double xmin, double ymin)
{
    double norm = 0.;

    for(size_t i = 1; i <= problem.nX(); ++i)
    {
        for(size_t j = 1; j <= problem.nY(); ++j)
        {
            norm += sqr( problem.phi(i, j) - solution(xmin + i*problem.gridStep(), ymin + j*problem.gridStep()) );
        }
    }

    return std::sqrt(sqr(problem.gridStep()) * norm);
}

inline void printResult(const Poisson2D& problem, Function2D solution, MultigridCycle cycle,
                        double xmin, double ymin, Milis time, bool perf = false)
{
    cout << "nx = " << problem.nX() << " ny = " << problem.nY() << ";";
    if(perf)
    {
        cout << "Gauss-Seidel perf test;";
    }
    else
    {
        cout << "cycle: " << cycle << ";";
    }
    cout << "|e| = "<< calculate2DL2Norm(problem, solution, xmin, ymin) << ";"
         << "steps total = " << problem.solverStepsTotal.value() << ";"
         << "duration[ms] = " << time.count() << endl;
}

inline void checkAndPrintResult(const Poisson2D& problem, Function2D solution, MultigridCycle cycle,
                                double xmin, double ymin, double tol, Milis time)
{
    assertResultNear(problem, solution, xmin, ymin, tol);
    printResult(problem, solution, cycle, xmin, ymin, time);
}

inline Milis solveWithTimer(Poisson2D& problem)
{
    auto before = Clock::now();
    problem.solve();
    auto after = Clock::now();
    return std::chrono::duration_cast<Milis>(after - before);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////// TEST FUNCTION CLASSES /////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class FunctionSet2DConstEps_1
{
public:
    static double density(double x, double y)
    {
        return 5*sin(x)*sin(2*y);
    }

    static double solution(double x, double y)
    {
        return -0.5*sin(x)*sin(2*y);
    }

    static double dervX(double x, double y)
    {
        return -0.5*cos(x)*sin(2*y);
    }
    static double dervY(double x, double y)
    {
        return -cos(2*y)*sin(x);
    }

    static double eps(double = 0, double = 0)
    {
        return -2;
    }

};

class FunctionSet2DConstEps_2
{
public:
    static double density(double x, double y)
    {
        return -2*(-1 + x)*x*(-1+y) - 4*(-1+x)*x*y - 2*(-1+y)*y*y;
    }

    static double solution(double x, double y)
    {
        return x*(x-1)*y*y*(y-1);
    }

    static double dervX(double x, double y)
    {
        return (-1+x)*(-1+y)*y*y + x*(-1+y)*y*y;
    }
    static double dervY(double x, double y)
    {
        return 2*(-1+x)*x*(-1+y)*y + (-1+x)*x*y*y;
    }

    static double eps(double = 0, double = 0)
    {
        return 1;
    }

};

class FunctionSet2DConstEps_3
{
public:
    static double density(double x, double y)
    {
        return 4*exp(-x*x -y*y) - 4*exp(-x*x -y*y)*x*x - 4*exp(-x*x -y*y)*y*y;
    }

    static double solution(double x, double y)
    {
        return exp(-(x*x + y*y));
    }

    static double dervX(double x, double y)
    {
        return -2*exp(-x*x -y*y)*x;
    }
    static double dervY(double x, double y)
    {
        return -2*exp(-x*x - y*y)*y;
    }

    static double eps(double = 0, double = 0)
    {
        return 1;
    }
};

class FunctionSet2DConstEps_4
{
public:
    static double density(double x, double y)
    {
        return 5*std::sin(x)*std::sin(2*y);
    }

    static double solution(double x, double y)
    {
        return std::sin(x)*std::sin(2*y);
    }

    static double dervX(double x, double y)
    {
        return cos(x)*sin(2*y);
    }
    static double dervY(double x, double y)
    {
        return 2*cos(2*y)*sin(x);
    }

    static double eps(double = 0, double = 0)
    {
        return 1;
    }
};

class FunctionSet2DConstEps_5
{
public:
    static double density(double x, double y)
    {
        return 4*exp(-sqr(x)-sqr(y)) -
               4*exp(-sqr(x)-sqr(y))*sqr(x) -
               4*exp(-sqr(x)-sqr(y))*sqr(y);
    }

    static double solution(double x, double y)
    {
        return exp(-sqr(x)-sqr(y));
    }

    static double dervX(double x, double y)
    {
        return -2*exp(-sqr(x)-sqr(y))*x;
    }
    static double dervY(double x, double y)
    {
        return -2*exp(-sqr(x)-sqr(y))*y;
    }

    static double eps(double = 0, double = 0)
    {
        return 1;
    }
};

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class FunctionSet2DVarEps_1
{
public:
    static double density(double x, double y)
    {
        return -2*cos(2*y)*sin(x) - cos(x)*sin(2*y) + 5*(1+x+y)*sin(x)*sin(2*y);
    }

    static double solution(double x, double y)
    {
        return sin(x)*sin(2*y);
    }

    static double dervX(double x, double y)
    {
        return cos(x)*sin(2*y);
    }
    static double dervY(double x, double y)
    {
        return 2*cos(2*y)*sin(x);
    }

    static double eps(double x, double y)
    {
        return 1. + x + y;
    }

};

class FunctionSet2DVarEps_2
{
public:
    static double density(double x, double y)
    {
        return exp( -pow(x, 2)*(1+pow(x, 4))-pow(y, 6) )
               * (2*x*(2+3*pow(x, 4))*cos(x) + (3-4*(x*x+3*pow(x, 6)) +6*pow(y, 4)*(5-6*pow(y, 6)))*sin(x) );
    }

    static double solution(double x, double y)
    {
        return sin(x)*exp(-pow(x, 2))*exp(-pow(y, 6));
    }

    static double dervX(double x, double y)
    {
        return exp(-pow(x, 2) -pow(y, 6))*(cos(x) - 2*x*sin(x));
    }
    static double dervY(double x, double y)
    {
        return -6*exp(-pow(x, 2) -pow(y, 6))*pow(y, 5)*sin(x);
    }

    static double eps(double x, double y)
    {
        return exp(-pow(x, 6));
    }

};

class FunctionSet2DVarEps_3
{
public:
    static double density(double x, double y)
    {
        return -4*x*(-1+2*x)*(-1+y)*sqr(y)
               - 2*(1+2*sqr(x))*(-1+y)*sqr(y)
               - 2*(-1+x)*x*(1+2*sqr(x))*(-1+3*y);
    }

    static double solution(double x, double y)
    {
        return x*(x-1)*sqr(y)*(y-1);
    }

    static double dervX(double x, double y)
    {
        return (-1+2*x)*(-1+y)*sqr(y);
    }
    static double dervY(double x, double y)
    {
        return (-1+x)*x*y*(-2+3*y);
    }

    static double eps(double x, double y)
    {
        return 1+2*sqr(x);
    }

};

}

#endif //POISSONAGH_TESTTOOLS2D_HPP
