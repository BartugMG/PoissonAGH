#include "gtest/gtest.h"
#include "PoissonAGH/Solver/SolverStrategy2D.hpp"
#include "TestTools2D.hpp"

namespace PoissonAGH
{
using namespace testing;
using namespace std;
using namespace utils;

namespace
{

struct ConditionsPack
{
    ConditionType2D north;
    ConditionType2D south;
    ConditionType2D west;
    ConditionType2D east;
    size_t iters;
    double error;
};

}

class SolverStrategy2DTestSuite1: public TestWithParam<ConditionsPack>
{};

TEST_P(SolverStrategy2DTestSuite1, shallSolveProblem)
{
    ConditionsPack params = GetParam();

    double xmin = 0;
    double xmax = 1;
    size_t nx = 63;
    auto h = getH(xmin, xmax, nx);

    Matrix2D rho(nx+2);
    Matrix2D phi(nx+2);

    fillMatrix2DWithFunction(rho, FunctionSet2DConstEps_1::density, xmin, xmin, h);
    applyConditionNorth<FunctionSet2DConstEps_1>(phi, params.north, xmax, xmin, h);
    applyConditionSouth<FunctionSet2DConstEps_1>(phi, params.south, xmin, xmin, h);
    applyConditionWest<FunctionSet2DConstEps_1>(phi, params.west, xmin, xmin, h);
    applyConditionEast<FunctionSet2DConstEps_1>(phi, params.east, xmin, xmax, h);

    auto solver = SolverGSStrategy2DFactory::createSolverConstEps(params.north, params.south, params.west, params.east,
                                                                  phi, rho, GridStep(h), FunctionSet2DConstEps_1::eps());

    for(size_t i = 0; i < params.iters; ++i)
    {
        solver->solve();
    }
    solver->findResidual(rho);

    checkResult<FunctionSet2DConstEps_1>(phi, xmin, xmin, h, params.error);
    checkResidual(rho, solver->findSumResidual(), -6504);
}

INSTANTIATE_TEST_CASE_P(NonHomogeneusNeumannConstEps,
                        SolverStrategy2DTestSuite1,
                        ::testing::Values(
                                // north, south, west, east, iterations, error
                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 5000, 1e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 8000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 8000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 8000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 8000, 1e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 18000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 18000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 18000, 2e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 18000, 2e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 18000, 2e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::NeumannEast, 28000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::NeumannEast, 29000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 28000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 28000, 2e-4}

                        ));
class SolverStrategy2DTestSuite2: public TestWithParam<ConditionsPack>
{};

TEST_P(SolverStrategy2DTestSuite2, shallSolveProblem)
{
    ConditionsPack params = GetParam();

    double xmin = 0;
    double xmax = 2;
    size_t nx = 127;
    double h = getH(xmin, xmax, nx);

    Matrix2D rho(nx+2);
    Matrix2D phi(nx+2);

    fillMatrix2DWithFunction(rho, FunctionSet2DConstEps_2::density, xmin, xmin, h);
    applyConditionNorth<FunctionSet2DConstEps_2>(phi, params.north, xmax, xmin, h);
    applyConditionSouth<FunctionSet2DConstEps_2>(phi, params.south, xmin, xmin, h);
    applyConditionWest<FunctionSet2DConstEps_2>(phi, params.west, xmin, xmin, h);
    applyConditionEast<FunctionSet2DConstEps_2>(phi, params.east, xmin, xmax, h);

    auto solver = SolverGSStrategy2DFactory::createSolverConstEps(params.north, params.south, params.west, params.east,
                                                                  phi, rho, GridStep(h), FunctionSet2DConstEps_2::eps());

    for(size_t i = 0; i < params.iters; ++i)
    {
        solver->solve();
    }
    solver->findResidual(rho);

    checkResult<FunctionSet2DConstEps_2>(phi, xmin, xmin, h, params.error);
    checkResidual(rho, solver->findSumResidual(), 42340);
}

INSTANTIATE_TEST_CASE_P(NonHomogeneusNeumannConstEps,
                        SolverStrategy2DTestSuite2,
                        ::testing::Values(
                                // north, south, west, east, iterations, error
                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 15000, 1e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 24000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 24000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 24000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 27000, 1e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 30000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 45000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 55000, 5e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 30000, 2e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 40000, 2e-4}

                                // No 3x Neumann 1x Dirichlet cpp_tests due to unreasonable amount of iterations
                        ));

class SolverStrategy2DTestSuite3: public TestWithParam<ConditionsPack>
{};

TEST_P(SolverStrategy2DTestSuite3, shallSolveProblem)
{
    ConditionsPack params = GetParam();

    double xmin = -1;
    double xmax = 1;
    size_t nx = 127;
    double h = getH(xmin, xmax, nx);

    Matrix2D rho(nx+2);
    Matrix2D phi(nx+2);

    fillMatrix2DWithFunction(rho, FunctionSet2DConstEps_3::density, xmin, xmin, h);
    applyConditionNorth<FunctionSet2DConstEps_3>(phi, params.north, xmax, xmin, h);
    applyConditionSouth<FunctionSet2DConstEps_3>(phi, params.south, xmin, xmin, h);
    applyConditionWest<FunctionSet2DConstEps_3>(phi, params.west, xmin, xmin, h);
    applyConditionEast<FunctionSet2DConstEps_3>(phi, params.east, xmin, xmax, h);

    auto solver = SolverGSStrategy2DFactory::createSolverConstEps(params.north, params.south, params.west, params.east,
                                                                  phi, rho, GridStep(h), FunctionSet2DConstEps_3::eps());

    for(size_t i = 0; i < params.iters; ++i)
    {
        solver->solve();
    }
    solver->findResidual(rho);

    checkResult<FunctionSet2DConstEps_3>(phi, xmin, xmin, h, params.error);
    checkResidual(rho, solver->findSumResidual(), -18073);
}

INSTANTIATE_TEST_CASE_P(NonHomogeneusNeumannConstEps,
                        SolverStrategy2DTestSuite3,
                        ::testing::Values(
                                // north, south, west, east, iterations, error
                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 18000, 1e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 25000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 25000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 25000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 25000, 1e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 32000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 60000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 60000, 2e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 60000, 2e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 60000, 2e-4}

                        ));

class SolverStrategy2DTestSuite4: public TestWithParam<ConditionsPack>
{};

TEST_P(SolverStrategy2DTestSuite4, shallSolveProblem)
{
    ConditionsPack params = GetParam();

    double xmin = -0.4;
    double xmax = 0.6;
    size_t nx = 63;
    double h = getH(xmin, xmax, nx);

    Matrix2D rho(nx+2);
    Matrix2D phi(nx+2);
    Matrix2D epsilon(nx+2);

    fillMatrix2DWithFunction(rho, FunctionSet2DVarEps_1::density, xmin, xmin, h);
    fillMatrix2DWithFunction(epsilon, FunctionSet2DVarEps_1::eps, xmin, xmin, h);
    applyConditionNorth<FunctionSet2DVarEps_1>(phi, params.north, xmax, xmin, h);
    applyConditionSouth<FunctionSet2DVarEps_1>(phi, params.south, xmin, xmin, h);
    applyConditionWest<FunctionSet2DVarEps_1>(phi, params.west, xmin, xmin, h);
    applyConditionEast<FunctionSet2DVarEps_1>(phi, params.east, xmin, xmax, h);

    auto solver = SolverGSStrategy2DFactory::createSolverVarEps(params.north, params.south, params.west, params.east,
                                                                phi, rho, GridStep(h), epsilon);

    for(size_t i = 0; i < params.iters; ++i)
    {
        solver->solve();
    }
    solver->findResidual(rho);

    checkResult<FunctionSet2DVarEps_1>(phi, xmin, xmin, h, params.error);
    checkResidual(rho, solver->findSumResidual(), 350);
}

INSTANTIATE_TEST_CASE_P(NonHomogeneusNeumannVarEps,
                        SolverStrategy2DTestSuite4,
                        ::testing::Values(
                                // north, south, west, east, iterations, error
                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 5000, 1e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 8000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 8000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 8000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 8000, 1e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 25000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 25000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 16000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 19000, 1e-4},

                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::NeumannEast, 28000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::NeumannEast, 40000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::NeumannEast, 40000, 2e-4},

                                ConditionsPack{ConditionType2D::NeumannNorth, ConditionType2D::NeumannSouth,
                                               ConditionType2D::NeumannWest, ConditionType2D::DirichletEast, 28000, 2e-4}
                        ));

class SolverStrategy2DTestSuite5: public TestWithParam<ConditionsPack>
{};

TEST_P(SolverStrategy2DTestSuite5, shallSolveProblem)
{
    ConditionsPack params = GetParam();

    double xmin = 0;
    double xmax = 2;
    size_t nx = 255;
    double h = getH(xmin, xmax, nx);

    Matrix2D rho(nx+2);
    Matrix2D phi(nx+2);
    Matrix2D epsilon(nx+2);

    fillMatrix2DWithFunction(rho, FunctionSet2DVarEps_2::density, xmin, xmin, h);
    fillMatrix2DWithFunction(epsilon, FunctionSet2DVarEps_2::eps, xmin, xmin, h);
    applyConditionNorth<FunctionSet2DVarEps_2>(phi, params.north, xmax, xmin, h);
    applyConditionSouth<FunctionSet2DVarEps_2>(phi, params.south, xmin, xmin, h);
    applyConditionWest<FunctionSet2DVarEps_2>(phi, params.west, xmin, xmin, h);
    applyConditionEast<FunctionSet2DVarEps_2>(phi, params.east, xmin, xmax, h);

    auto solver = SolverGSStrategy2DFactory::createSolverVarEps(params.north, params.south, params.west, params.east,
                                                                phi, rho, GridStep(h), epsilon);

    for(size_t i = 0; i < params.iters; ++i)
    {
        solver->solve();
    }
    solver->findResidual(rho);

    checkResult<FunctionSet2DVarEps_2>(phi, xmin, xmin, h, params.error);
}

INSTANTIATE_TEST_CASE_P(NonHomogeneusNeumannVarEps,
                        SolverStrategy2DTestSuite5,
                        ::testing::Values(
                                // north, south, west, east, iterations, error
                                ConditionsPack{ConditionType2D::DirichletNorth, ConditionType2D::DirichletSouth,
                                               ConditionType2D::DirichletWest, ConditionType2D::DirichletEast, 30000, 1e-2}
                        ));

}
