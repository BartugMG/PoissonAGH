#ifndef POISSONAGH_POISSON1D_C_H
#define POISSONAGH_POISSON1D_C_H
#include <cstddef>

#ifdef __cplusplus
extern "C" {
#endif

typedef void* Poisson1D_Ptr;
typedef double (*PFunction1D)(double);

// construction and destruction
Poisson1D_Ptr P1D_new_Poisson1D_step(double xmin, double xmax, double h, PFunction1D densityFunc);
Poisson1D_Ptr P1D_new_Poisson1D_size(double xmin, double xmax, size_t nx, PFunction1D densityFunc);
Poisson1D_Ptr P1D_new_Poisson1D_array(double* rho_arr, size_t len, double h);
void P1D_delete_Poisson1D(Poisson1D_Ptr solver);

int P1D_isSolved(Poisson1D_Ptr solver);

double P1D_getXmin(Poisson1D_Ptr solver);
double P1D_getXmax(Poisson1D_Ptr solver);
size_t P1D_getNx(Poisson1D_Ptr solver);
double P1D_getStep(Poisson1D_Ptr solver);

double P1D_getEpsilonC(Poisson1D_Ptr solver);
void P1D_setEpsilonC(Poisson1D_Ptr solver, double epsilonC);

const double* P1D_getEpsilonArray(Poisson1D_Ptr solver);
void P1D_setEpsilonArray(Poisson1D_Ptr solver, double* eps_arr);

const double* P1D_getPhiArray(Poisson1D_Ptr solver);
const double* P1D_getRhoArray(Poisson1D_Ptr solver);

double P1D_getLeftBoundaryValue(Poisson1D_Ptr solver);
double P1D_getRightBoundaryValue(Poisson1D_Ptr solver);
void P1D_setBoundaryValues(Poisson1D_Ptr solver, double left, double right);

enum P1D_ConditionType { DIRICHLET_DIRICHLET, DIRICHLET_NEUMANN, NEUMANN_DIRICHLET, PERIODIC };
P1D_ConditionType P1D_getConditionType(Poisson1D_Ptr solver);
void P1D_setConditionsType(Poisson1D_Ptr solver, P1D_ConditionType conditionType);

void P1D_solve(Poisson1D_Ptr solver);

#ifdef __cplusplus
}
#endif

#endif //POISSONAGH_POISSON1D_C_H
