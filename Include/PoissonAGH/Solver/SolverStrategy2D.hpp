#pragma once

#include "PoissonAGH/Utils/PTools.hpp"
#include "PoissonAGH/Utils/PTypes.hpp"
#include "PoissonAGH/Utils/PMatrix.hpp"

/**
 * @addtogroup PoissonAGH
 * @{
 */
namespace PoissonAGH
{

/**
 * @class ConditionType2D
 *
 * Enum containing all possible variations of boundary conditions types (Dirichlet, Neumann), and grid edges sides
 * (North, South, West, East).
 */
enum class ConditionType2D { DirichletNorth, DirichletSouth, DirichletWest, DirichletEast,
                             NeumannNorth, NeumannSouth, NeumannWest, NeumannEast};

/**
 * @addtogroup utils
 * @{
 */
namespace utils
{
/**
 * @class ISolverStrategy2D
 *
 * Base class for all 2D iterative solvers, which can be used as the heart of the multigrid method.
 * Besides solver, method for finding residual is also necessary.
 */
class ISolverStrategy2D
{
public:
    virtual ~ISolverStrategy2D() = default;
    /**
     * Single iterative method solver step.
     *
     * @param u 2D matrix with initial result, which will be corrected by method
     * @param f 2D matrix with charge density (values from function which we try to solve)
     * @param h Gird step
     */
    virtual void solve() noexcept = 0;
    /**
     * Method for finding residual: r = f - Av.
     * Assumption is that on begin r == f
     *
     * @param r Residual to find, charge density on beginning
     * @param u Result from solve function
     * @param h Grid step
     */
    virtual void findResidual(Matrix2D& r) noexcept = 0;
    virtual double findSumResidual() noexcept = 0;
};

using Solver2DPtr = std::unique_ptr<ISolverStrategy2D>;

/**
 * @class IGaussSeidelSolverStrategy2D
 *
 * Base class for all red-black Gauss Seidel 2D iterative solvers for multigrid method.
 */
class IGaussSeidelSolverStrategy2D: public ISolverStrategy2D
{
public:
    ~IGaussSeidelSolverStrategy2D() override = default;
protected:
    enum Colour { RED = 0, BLACK = 1 };
    virtual void gaussSeidelOneColour(Colour colour) noexcept = 0;
};

/**
 * @class AllDirichletGSConstEps2D
 *
 * Class implementing solver with red-black Gauss Seidel method and all Dirichlet boundary conditions for constant
 * absolute permittivity.
 */
class AllDirichletGSConstEps2D: public IGaussSeidelSolverStrategy2D
{
public:
    AllDirichletGSConstEps2D(Matrix2D& u, const Matrix2D& f, GridStep h, double eps):
        u(u), f(f), h(h), eps(eps), rows(u.rows()-2), cols(u.cols()-2)
    {}
    void solve() noexcept override;
    void findResidual(Matrix2D& r) noexcept override;
    double findSumResidual() noexcept override;

private:
    Matrix2D& u;
    const Matrix2D& f;
    const GridStep h;
    const double eps;
    const size_t rows;
    const size_t cols;
    void gaussSeidelOneColour(Colour colour) noexcept override;
};

/**
 * @class NeumannSolverGSConstEps2D
 *
 * Class implementing solver with red-black Gauss Seidel method and one or more Neumann boundary conditions for
 * constant absolute permittivity.
 */
class NeumannSolverGSConstEps2D: public IGaussSeidelSolverStrategy2D
{
public:
    NeumannSolverGSConstEps2D(bool northNeumann, bool southNeumann, bool westNeumann, bool eastNeumann,
                              Matrix2D& u, const Matrix2D& f, GridStep h, double eps);

    void solve() noexcept override;

    void findResidual(Matrix2D& r) noexcept override;
    double findSumResidual() noexcept override;

private:
    Matrix2D& u;
    const Matrix2D& f;
    const GridStep h;
    const double eps;
    const size_t rows;
    const size_t cols;

    GridStep hMinNorth;
    GridStep hMaxNorth;
    GridStep hMinSouth;
    GridStep hMaxSouth;
    GridStep hMinWest;
    GridStep hMaxWest;
    GridStep hMinEast;
    GridStep hMaxEast;

    void gaussSeidelOneColour(Colour colour) noexcept override;

    void solveDirichlet(Matrix2D& u, size_t i, size_t j) const noexcept;
    void solveNeumann(Matrix2D& u, size_t i, size_t j, GridStep hXmin, GridStep hXmax, GridStep hYmin, GridStep hYmax) const noexcept;

    void findRDirichlet(Matrix2D& r, size_t i, size_t j) const noexcept;
    void findRNeumann(Matrix2D& r, size_t i, size_t j, GridStep hXmin, GridStep hXmax, GridStep hYmin, GridStep hYmax) const noexcept;

    void findSumRDirichlet(double& r, size_t i, size_t j) const noexcept;
    void findSumRNeumann(double& r, size_t i, size_t j, GridStep hXmin, GridStep hXmax, GridStep hYmin, GridStep hYmax) const noexcept;

    template<typename F1, typename F2, typename U>
    void generalSolverTemplate(F1 nFunction, F2 dFunction, U& variable, Colour colour) const noexcept
    {
        if(colour == Colour::RED)
        {
            (this->*nFunction)(variable, 1, 1, hMinSouth, hMaxSouth, hMinWest, hMaxWest);
            (this->*nFunction)(variable, 1, cols, hMinSouth, hMaxSouth, hMinEast, hMaxEast);
            (this->*nFunction)(variable, rows, 1, hMinNorth, hMaxNorth, hMinWest, hMaxWest);
            (this->*nFunction)(variable, rows, cols, hMinNorth, hMaxNorth, hMinEast, hMaxEast);
        }

        for(size_t i = 3u-colour; i < rows; i+=2)
        {
            (this->*nFunction)(variable, i, 1, GridStep(1.), GridStep(1.), hMinWest, hMaxWest);
            (this->*nFunction)(variable, i, cols, GridStep(1.), GridStep(1.), hMinEast, hMaxEast);
        }

        for(size_t j = 3u-colour; j < cols; j+=2)
        {
            (this->*nFunction)(variable, 1, j, hMinSouth, hMaxSouth, GridStep(1.), GridStep(1.));
            (this->*nFunction)(variable, rows, j, hMinNorth, hMaxNorth, GridStep(1.), GridStep(1.));
        }

        for(size_t i = 2; i < rows; ++i)
        {
            for(size_t j = 3 - (i + colour) % 2; j < cols; j += 2)
            {
                (this->*dFunction)(variable, i, j);
            }
        }

    }
};

/**
 * @class AllDirichletGSVarEps2D
 *
 * Class implementing solver with red-black Gauss Seidel method and all Dirichlet boundary conditions for variable
 * absolute permittivity given as 2D matrix.
 */
class AllDirichletGSVarEps2D : public IGaussSeidelSolverStrategy2D
{
public:
    AllDirichletGSVarEps2D(Matrix2D& u, const Matrix2D& f, GridStep h, const Matrix2D& eps):
            u(u), f(f), h(h), eps(eps), rows(u.rows()-2), cols(u.cols()-2)
    {}

    void solve() noexcept override;
    void findResidual(Matrix2D& r) noexcept override;
    double findSumResidual() noexcept override;
private:
    Matrix2D& u;
    const Matrix2D& f;
    const GridStep h;
    const Matrix2D& eps;
    const size_t rows;
    const size_t cols;

    void gaussSeidelOneColour(Colour colour) noexcept override;
};

/**
 * @class NeumannSolverGSVarEps2D
 *
 * Class implementing solver with red-black Gauss Seidel method and one or more Neumann boundary conditions for
 * variable absolute permittivity gives as 2D matrix.
 */
class NeumannSolverGSVarEps2D: public IGaussSeidelSolverStrategy2D
{
    typedef double (NeumannSolverGSVarEps2D::*PartialSolver)(Matrix2D& u, size_t, size_t);
public:
    NeumannSolverGSVarEps2D(bool isNorth, bool isSouth, bool isWest, bool isEast,
                            Matrix2D& u, const Matrix2D& f, GridStep h, const Matrix2D& eps);

    void solve() noexcept override;
    void findResidual(Matrix2D& r) noexcept override;
    double findSumResidual() noexcept override;

private:
    Matrix2D& u;
    const Matrix2D& f;
    const GridStep h;
    const Matrix2D& eps;
    const size_t rows;
    const size_t cols;

    PartialSolver northNeumann;
    PartialSolver southNeumann;
    PartialSolver westNeumann;
    PartialSolver eastNeumann;

    PartialSolver northDirichlet;
    PartialSolver southDirichlet;
    PartialSolver westDirichlet;
    PartialSolver eastDirichlet;

    void gaussSeidelOneColour(Colour colour) noexcept override;
    double getSumEps(size_t i, size_t j) const noexcept;

    void solveDirichlet(Matrix2D&u, size_t i, size_t j) noexcept;
    template<typename F>
    void solveNeumann(Matrix2D& u, size_t i, size_t j, F north, F south, F west, F east) noexcept
    {
        u(i, j) = ((this->*north)(u, i, j) + (this->*south)(u, i, j) + (this->*west)(u, i, j) + (this->*east)(u, i, j)
                   +2*h.value()*h.value()*f(i, j)) / getSumEps(i, j);
    }

    void findRDirichlet(Matrix2D& r, size_t i, size_t j) const noexcept;

    template<typename F>
    void findRNeumann(Matrix2D& r, size_t i, size_t j, F north, F south, F west, F east) noexcept
    {
        r(i, j) += ((this->*north)(u, i, j) + (this->*south)(u, i, j) + (this->*west)(u, i, j) + (this->*east)(u, i, j)
                    - getSumEps(i, j)*u(i, j)) / (2*h.value()*h.value());
    }

    void findSumRDirichlet(double& r, size_t i, size_t j) const noexcept;

    template<typename F>
    void findSumRNeumann(double& r, size_t i, size_t j, F north, F south, F west, F east) noexcept
    {
        r += f(i, j) + ((this->*north)(u, i, j) + (this->*south)(u, i, j) + (this->*west)(u, i, j) + (this->*east)(u, i, j)
                        - getSumEps(i, j)*u(i, j)) / (2*h.value()*h.value());
    }

    template<int s_i, int s_j>
    double createPartialDirichletSolver(Matrix2D& u, size_t i, size_t j) noexcept
    {
        return (eps(i+s_i, j+s_j)+eps(i, j))*u(i+s_i, j+s_j);
    }

    template<int s_i, int s_j>
    double createPartialNeumannSolver(Matrix2D& u, size_t i, size_t j) noexcept
    {
        return (eps(i+s_i, j+s_j)+eps(i, j))*((s_i+s_j)*2*h.value()*u(i+s_i, j+s_j) + u(i-s_i, j-s_j));
    }

    template<typename F1, typename F2, typename U>
    void generalSolverTemplate(F1 nFunction, F2 dFunction, U& variable, Colour colour) noexcept
    {
        if(colour == Colour::RED)
        {
            (this->*nFunction)(variable, 1, 1, northDirichlet, southNeumann, westNeumann, eastDirichlet);
            (this->*nFunction)(variable, 1, cols, northDirichlet, southNeumann, westDirichlet, eastNeumann);
            (this->*nFunction)(variable, rows, 1, northNeumann, southDirichlet, westNeumann, eastDirichlet);
            (this->*nFunction)(variable, rows, cols, northNeumann, southDirichlet, westDirichlet, eastNeumann);
        }

        for(size_t i = 3u-colour; i < rows; i+=2)
        {
            (this->*nFunction)(variable, i, 1, northDirichlet, southDirichlet, westNeumann, eastDirichlet);
            (this->*nFunction)(variable, i, cols, northDirichlet, southDirichlet, westDirichlet, eastNeumann);
        }

        for(size_t j = 3u-colour; j < cols; j+=2)
        {
            (this->*nFunction)(variable, 1, j, northDirichlet, southNeumann, westDirichlet, eastDirichlet);
            (this->*nFunction)(variable, rows, j, northNeumann, southDirichlet, westDirichlet, eastDirichlet);
        }

        for(size_t i = 2; i < rows; ++i)
        {
            for(size_t j = 3 - (i + colour) % 2; j < cols; j += 2)
            {
                (this->*dFunction)(variable, i, j);
            }
        }
    }


};

/**
 * @class SolverGSStrategy2DFactory
 *
 * Factory class for creating all types of 2D solvers.
 */
class SolverGSStrategy2DFactory
{
public:
    static Solver2DPtr createSolverConstEps(ConditionType2D north, ConditionType2D south,
                                            ConditionType2D west, ConditionType2D east,
                                            Matrix2D& u, const Matrix2D& f, GridStep h, double eps);

    static Solver2DPtr createSolverVarEps(ConditionType2D north, ConditionType2D south,
                                          ConditionType2D west, ConditionType2D east,
                                          Matrix2D& u, const Matrix2D& f, GridStep h, const Matrix2D& eps);
};

}
/** @} End of group */

}
/** @} End of group */

