#pragma once

#include "PoissonAGH/Utils/PTypes.hpp"
#include <vector>

namespace PoissonAGH
{
using Array1D = std::vector<double>;

// TODO remove
using WrapperType = void(*)(Array1D&, Array1D&, Array1D&, Array1D&);

struct Sparse
{
    Sparse() = default;

    explicit Sparse(DimensionSize n):
        DU(n), D(n), DL(n)
    {}

    Array1D DU;
    Array1D D;
    Array1D DL;
};

/**
 * @class Matrix
 * @tparam T Internal matrix type
 *
 * Custom implementations of 2D matrix as 1D vector of values,
 * with some useful methods and operators.
 */
template<typename T>
class PMatrix2D
{
public:
    PMatrix2D(): m_rows(0), m_cols(0)
    {}

    explicit PMatrix2D(size_t size):
        m_rows(size), m_cols(size), data(m_rows*m_cols)
    {}

    PMatrix2D(size_t height, size_t width):
        m_rows(height), m_cols(width), data(m_rows*m_cols)
    {}

    PMatrix2D(const PMatrix2D& rhs):
        m_rows(rhs.m_rows), m_cols(rhs.m_cols), data(rhs.data)
    {}

    PMatrix2D(PMatrix2D&& rhs) noexcept:
        m_rows(rhs.m_rows), m_cols(rhs.m_cols), data(std::move(rhs.data))
    {}

    PMatrix2D(const T* array, size_t lenX, size_t lenY):
        m_rows(lenX), m_cols(lenY), data(array, array+m_rows*m_cols)
    {}

    PMatrix2D(T** array, size_t lenX, size_t lenY):
        m_rows(lenX), m_cols(lenY), data(m_rows*m_cols)
    {
        for(size_t i = 0; i < m_rows; ++i)
        {
            for(size_t j = 0; j < m_cols; ++j)
            {
                data[j + i*m_cols] = array[i][j];
            }
        }
    }

    PMatrix2D& operator=(const PMatrix2D& rhs) = default;
    PMatrix2D& operator=(PMatrix2D&& rhs) noexcept = default;

    void fill(T value)
    {
        std::fill(std::begin(data), std::end(data), value);
    }

    bool operator==(const PMatrix2D& rhs) const
    {
        if(m_rows != rhs.m_rows or m_cols != rhs.m_cols)
        {
            return false;
        }
        return std::equal(data.begin(), data.end(), rhs.data.begin());
    }

    T operator()(size_t i, size_t j) const { return data[j + i*m_cols]; }
    T& operator()(size_t i, size_t j) { return data[j + i*m_cols]; }

    const T* getData() const { return data.data(); }

    size_t rows() const { return m_rows; }
    size_t cols() const { return m_cols; }

private:
    size_t m_rows;
    size_t m_cols;
    std::vector<T> data;
};

using Matrix2D = PMatrix2D<double>;

/**
 * @class Matrix3D
 *
 * Custom implementations of 3D matrix as 1D vector of doubles,
 * with some useful methods and operators.
 */
template <typename T>
class PMatrix3D
{
public:
    PMatrix3D() : m_rows(0), m_cols(0), m_slices(0) {}

    explicit PMatrix3D(size_t size):
        m_rows(size), m_cols(size), m_slices(size), m_data(m_rows*m_cols*m_slices)
    {}

    PMatrix3D(size_t height, size_t width, size_t depth):
        m_rows(height), m_cols(width), m_slices(depth), m_data(m_rows*m_cols*m_slices)
    {}

    PMatrix3D(const PMatrix3D& rhs) = default;

    PMatrix3D(PMatrix3D&& rhs) noexcept :
        m_rows(rhs.m_rows), m_cols(rhs.m_cols), m_slices(rhs.m_slices),
        m_data(std::move(rhs.m_data))
    {}

    PMatrix3D(const T* array, size_t lenX, size_t lenY, size_t lenZ):
            m_rows(lenX), m_cols(lenY), m_slices(lenZ), m_data(array, array+lenX*lenY*lenZ)
    {}

    PMatrix3D(T*** array, size_t lenX, size_t lenY, size_t lenZ):
            m_rows(lenX), m_cols(lenY), m_slices(lenZ), m_data(lenX*lenY*lenZ)
    {
        for(size_t i = 0; i < m_rows; ++i)
        {
            for(size_t j = 0; j < m_cols; ++j)
            {
                for(size_t k = 0; k < m_slices; ++k)
                {
                    m_data[k + (j + i*m_cols)*m_slices] = array[i][j][k];
                }
            }
        }
    }

    PMatrix3D& operator=(const PMatrix3D& rhs) = default;
    PMatrix3D& operator=(PMatrix3D&& rhs) noexcept = default;

    bool operator==(const PMatrix3D& rhs) const
    {
        if(m_rows != rhs.m_rows or m_cols != rhs.m_cols or m_slices != rhs.m_slices)
        {
            return false;
        }
        return std::equal(m_data.begin(), m_data.end(), rhs.m_data.begin());
    }

    T operator()(size_t i, size_t j, size_t k) const
    { return m_data[k + (j + i * m_cols) * m_slices]; }

    T& operator()(size_t i, size_t j, size_t k)
    { return m_data[k + (j + i * m_cols) * m_slices]; }

    const T* getData() const { return m_data.data(); }

    size_t rows() const { return m_rows; }
    size_t cols() const { return m_cols; }
    size_t slices() const { return m_slices; }

private:
    size_t m_rows;
    size_t m_cols;
    size_t m_slices;
    Array1D m_data;

};

using Matrix3D = PMatrix3D<double>;

}
