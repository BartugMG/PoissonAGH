#ifndef POISSONAGH_POISSON3D_C_H
#define POISSONAGH_POISSON3D_C_H
#include <cstddef>

#ifdef __cplusplus
extern "C" {
#endif

typedef void* Poisson3D_Ptr;

// construction and destruction
Poisson3D_Ptr P3D_new_Poisson3D_cubeStep(double xmin, double xmax, double h);
Poisson3D_Ptr P3D_new_Poisson3D_cubeSize(double xmin, double xmax, size_t nx);
Poisson3D_Ptr P3D_new_Poisson3D_cuboidalStep(double xmin, double xmax, double ymin, double ymax,
                                             double zmin, double zmax, double h);
Poisson3D_Ptr P3D_new_Poisson3D_matrix(double*** rho_arr, size_t rows, size_t cols, size_t slices, double h);
Poisson3D_Ptr P3D_new_Poisson3D_array(double* rho_arr, size_t rows, size_t cols, size_t slices, double h);
void P3D_delete_Poisson3D(Poisson3D_Ptr solver);

// Poisson data access
int P3D_isSolved(Poisson3D_Ptr solver);

double P3D_getXmin(Poisson3D_Ptr solver);
double P3D_getXmax(Poisson3D_Ptr solver);
double P3D_getYmin(Poisson3D_Ptr solver);
double P3D_getYmax(Poisson3D_Ptr solver);
double P3D_getZmin(Poisson3D_Ptr solver);
double P3D_getZmax(Poisson3D_Ptr solver);
size_t P3D_getNx(Poisson3D_Ptr solver);
size_t P3D_getNy(Poisson3D_Ptr solver);
size_t P3D_getNz(Poisson3D_Ptr solver);
double P3D_getStep(Poisson3D_Ptr solver);

double P3D_getEpsilonC(Poisson3D_Ptr solver);
void P3D_setEpsilonC(Poisson3D_Ptr solver, double epsilonC);

// Poisson3D data access
const double* P3D_getEpsilonMatrix(Poisson3D_Ptr solver);
void P3D_setEpsilonMatrix(Poisson3D_Ptr solver, const double* eps_matrix);

const double* P3D_getPhiMatrix(Poisson3D_Ptr solver);
const double* P3D_getRhoMatrix(Poisson3D_Ptr solver);

void P3D_setSolverSteps(Poisson3D_Ptr solver, size_t steps);
void P3D_setMaxMultigridCalls(Poisson3D_Ptr solver, size_t calls);
void P3D_setResidualDelta(Poisson3D_Ptr solver, double delta);

enum P3D_ConditionType3D { DIRICHLET_FRONT, DIRICHLET_BACK, DIRICHLET_UP, DIRICHLET_DOWN, DIRICHLET_LEFT, DIRICHLET_RIGHT,
                           NEUMANN_FRONT, NEUMANN_BACK, NEUMANN_UP, NEUMANN_DOWN, NEUMANN_LEFT, NEUMANN_RIGHT};
P3D_ConditionType3D P3D_getFrontCondition(Poisson3D_Ptr solver);
P3D_ConditionType3D P3D_getBackCondition(Poisson3D_Ptr solver);
P3D_ConditionType3D P3D_getUpCondition(Poisson3D_Ptr solver);
P3D_ConditionType3D P3D_getDownCondition(Poisson3D_Ptr solver);
P3D_ConditionType3D P3D_getLeftCondition(Poisson3D_Ptr solver);
P3D_ConditionType3D P3D_getRightCondition(Poisson3D_Ptr solver);
void P3D_setCondition(Poisson3D_Ptr solver, const double* values, size_t lenX, size_t lenY, P3D_ConditionType3D condition);

enum P3D_MultigridType { VCYCLE, WCYCLE, FULL_MULTIGRID };
void P3D_setMultigridType(Poisson3D_Ptr solver, P3D_MultigridType multigridType);

void P3D_solve(Poisson3D_Ptr solver);

// Utility
/**
 * Since library store data as 1D double arrays, index shifts are needed when we want
 * to get data as if it would be 3D array. To achieve this, we need also amount of
 * all columns and all slices.
 *
 * @param matrix 3D matrix, represented as 1D double array
 * @param columns Amount of columns in matrix
 * @param slices Amount of slices in matrix
 * @param i Axis X index (row)
 * @param j Axis Y index (column)
 * @param k Axis Z index (slice)
 * @return double Value at position matrix[i][j][k]
 */
double P3D_getValueAt(const double* matrix, size_t columns, size_t slices, size_t i, size_t j, size_t k);

/**
 * @param matrix 3D matrix, represented as 1D double array
 * @param value A value we want to put inside matrix
 * @param columns Amount of columns in matrix
 * @param slices Amount of slices in matrix
 * @param i Axis X index (row)
 * @param j Axis Y index (column)
 * @param k Axis Z index (slice)
 */
void P3D_setValueAt(double* matrix, double value, size_t columns, size_t slices, size_t i, size_t j, size_t k);

#ifdef __cplusplus
}
#endif
#endif //POISSONAGH_POISSON3D_C_H
