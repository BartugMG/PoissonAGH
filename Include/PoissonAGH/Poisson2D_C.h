#ifndef POISSONAGH_POISSON2D_C_H
#define POISSONAGH_POISSON2D_C_H
#include <cstddef>

#ifdef __cplusplus
extern "C" {
#endif

typedef void* Poisson2D_Ptr;

// construction and destruction
Poisson2D_Ptr P2D_new_Poisson2D_squareStep(double xmin, double xmax, double h);
Poisson2D_Ptr P2D_new_Poisson2D_squareSize(double xmin, double xmax, size_t nx);
Poisson2D_Ptr P2D_new_Poisson2D_rectangleStep(double xmin, double xmax, double ymin, double ymax, double h);
Poisson2D_Ptr P2D_new_Poisson2D_matrix(double** rho_arr, size_t rows, size_t cols, double h);
Poisson2D_Ptr P2D_new_Poisson2D_array(double* rho_arr, size_t rows, size_t cols, double h);
void P2D_delete_Poisson2D(Poisson2D_Ptr solver);

// Poisson data access
int P2D_isSolved(Poisson2D_Ptr solver);

double P2D_getXmin(Poisson2D_Ptr solver);
double P2D_getXmax(Poisson2D_Ptr solver);
double P2D_getYmin(Poisson2D_Ptr solver);
double P2D_getYmax(Poisson2D_Ptr solver);
size_t P2D_getNx(Poisson2D_Ptr solver);
size_t P2D_getNy(Poisson2D_Ptr solver);
double P2D_getStep(Poisson2D_Ptr solver);

double P2D_getEpsilonC(Poisson2D_Ptr solver);
void P2D_setEpsilonC(Poisson2D_Ptr solver, double epsilonC);

// Poisson2D data access
const double* P2D_getEpsilonMatrix(Poisson2D_Ptr solver);
void P2D_setEpsilonMatrix(Poisson2D_Ptr solver, const double* eps_matrix);

const double* P2D_getPhiMatrix(Poisson2D_Ptr solver);
const double* P2D_getRhoMatrix(Poisson2D_Ptr solver);

void P2D_setSolverSteps(Poisson2D_Ptr solver, size_t steps);
void P2D_setMaxMultigridCalls(Poisson2D_Ptr solver, size_t calls);
void P2D_setResidualDelta(Poisson2D_Ptr solver, double delta);

enum P2D_ConditionType2D { DIRICHLET_NORTH, DIRICHLET_SOUTH, DIRICHLET_WEST, DIRICHLET_EAST,
                           NEUMANN_NORTH, NEUMANN_SOUTH, NEUMANN_WEST, NEUMANN_EAST };
P2D_ConditionType2D P2D_getNorthCondition(Poisson2D_Ptr solver);
P2D_ConditionType2D P2D_getSouthCondition(Poisson2D_Ptr solver);
P2D_ConditionType2D P2D_getWestCondition(Poisson2D_Ptr solver);
P2D_ConditionType2D P2D_getEastCondition(Poisson2D_Ptr solver);
void P2D_setCondition(Poisson2D_Ptr solver, const double* values, size_t len, P2D_ConditionType2D condition);

enum P2D_MultigridType { VCYCLE, WCYCLE, FULL_MULTIGRID };
void P2D_setMultigridType(Poisson2D_Ptr solver, P2D_MultigridType multigridType);

void P2D_solve(Poisson2D_Ptr solver);

// Utility
/**
 * Since library store data as 1D double arrays, index shifts are needed when we want
 * to get data as if it would be 2D array. To achieve this, we need also amount of
 * all columns.
 *
 * @param matrix 2D matrix, represented as 1D double array
 * @param columns Amount of columns in matrix
 * @param i Axis X index (row)
 * @param j Axis Y index (column)
 * @return double Value at position matrix[i][j]
 */
double P2D_getValueAt(const double* matrix, size_t columns, size_t i, size_t j);

/**
 * @param matrix 2D matrix, represented as 1D double array
 * @param value A value we want to put inside matrix
 * @param columns Amount of columns in matrix
 * @param i Axis X index (row)
 * @param j Axis Y index (column)
 */
void P2D_setValueAt(double* matrix, double value, size_t columns, size_t i, size_t j);

#ifdef __cplusplus
}
#endif

#endif //POISSONAGH_POISSON2D_C_H
