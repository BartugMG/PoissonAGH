#include "PoissonAGH/Poisson2D.hpp"
#include <iostream>

using namespace std;

namespace PoissonAGH
{
using namespace utils;

#ifdef WIP
void Poisson2D::init()
{
    if(m_nX % 2 == 0 or m_nY % 2 == 0)
    {
        throw std::logic_error("Size of mesh must be odd number");
    }

    if(northCondition == ConditionType2D::NeumannNorth and
       southCondition == ConditionType2D::NeumannSouth and
       westCondition == ConditionType2D::NeumannWest and
       eastCondition == ConditionType2D::NeumannEast)
    {
        throw std::logic_error("Neumann condtions can't be on every edge!");
    }

    if(epsilonMatrixGiven and (rho.rows() != epsilonMatrix.rows() or
                               rho.cols() != epsilonMatrix.cols()))
    {
        throw std::logic_error("Size of epsilon matrix not equal to rho matrix");
    }

    if(not (isPowerOf2(m_nX+1) and isPowerOf2(m_nY+1)) )
    {
        cout << "Sizes of given inner mesh aren't 2^n-1, solution might not be optimal" << endl;
    }

}


void Poisson2D::restrict(Matrix2D& R, const Matrix2D& r)
{
    const size_t rows = R.rows() - 2;
    const size_t cols = R.cols() - 2;

    for(size_t I = 1; I <= rows; ++I)
    {
        const size_t i = 2*I;
        for(size_t J = 1; J <= cols; ++J)
        {
            const size_t j = 2*J;
            R(I, J) = 0.0625 * (r(i-1, j-1) + r(i-1, j+1) + r(i+1, j-1) + r(i+1, j+1) +
                                2 * (r(i-1, j) + r(i, j-1) + r(i+1, j) + r(i, j+1)) +
                                4 * r(i, j));
        }
    }
}

void Poisson2D::restrictSimple(Matrix2D& R, const Matrix2D& r)
{
    const size_t rows = R.rows() - 2;
    const size_t cols = R.cols() - 2;

    for(size_t I = 1; I <= rows; ++I)
    {
        size_t i = 2*I;
        for(size_t J = 1; J <= cols; ++J)
        {
            size_t j = 2*J;
            R(I, J) = r(i, j);
        }
    }
    restrictBorders(R, r);
}

void Poisson2D::restrictBorders(Matrix2D& R, const Matrix2D& r)
{
    for(size_t I = 0; I < R.rows(); ++I)
    {
        const size_t i = 2*I;
        R(I, R.cols()-1) = r(i, r.cols()-1);
        R(I, 0) = r(i, 0);
    }

    for(size_t J = 0; J < R.cols(); ++J)
    {
        const size_t j = 2*J;
        R(0, J) = r(0, j);
        R(R.rows()-1, J) = r(r.rows()-1, j);
    }
}

void Poisson2D::prolongate(Matrix2D& v, const Matrix2D& V)
{
    const size_t rows = V.rows() - 2;
    const size_t cols = V.cols() - 2;

    for(size_t I = 1; I <= rows; ++I)
    {
        const size_t i = 2*I;
        for(size_t J = 1; J <= cols; ++J)
        {
            const size_t j = 2*J;
            v(i, j) = V(I, J);

            v(i-1, j) = 0.5 * (V(I, J) + V(I-1, J));
            v(i+1, j) = 0.5 * (V(I, J) + V(I+1, J));
            v(i, j-1) = 0.5 * (V(I, J) + V(I, J-1));
            v(i, j+1) = 0.5 * (V(I, J) + V(I, J+1));

            v(i-1, j-1) = 0.25 * (V(I, J) + V(I-1, J) + V(I, J-1) + V(I-1, J-1));
            v(i+1, j+1) = 0.25 * (V(I, J) + V(I+1, J) + V(I, J+1) + V(I+1, J+1));
            v(i+1, j-1) = 0.25 * (V(I, J) + V(I, J-1) + V(I+1, J) + V(I+1, J-1));
            v(i-1, j+1) = 0.25 * (V(I, J) + V(I, J+1) + V(I-1, J) + V(I-1, J+1));
        }
    }
}

template<bool W_CYCLE>
void Poisson2D::basicCycle(Matrix2D& u, const Matrix2D& f, GridStep m_h, const Matrix2D& epsMatrix,
                           const Solver2DPtr& solverStrategy)
{
    const size_t rows = u.rows() - 2;
    const size_t cols = u.cols() - 2;
    //cout << rows << " " << cols << " " << m_h << endl;

    /// do pre-smoothing Gauss-Seidel steps
    for(Steps i{0}; i < solverSteps; ++i)
    {
        solverStrategy->solve();
    }
    solverStepsTotal += solverSteps;

    /// solve exactly if there is only one interior point or number of points divisible by 2
    if(rows == 1 or cols == 1 or
       rows % 2 == 0 or cols % 2 == 0)
    {
        solverStrategy->solve();
        ++solverStepsTotal;
        return;
    }

    /// find the residual
    Matrix2D r = f;
    solverStrategy->findResidual(r);

    /// restricting to coarser grid
    size_t coarseRows = rows / 2 + 2;
    size_t coarseCols = cols / 2 + 2;
    Matrix2D R(coarseRows, coarseCols);
    restrict(R, r);

    /// initialize correction V on coarse grid
    Matrix2D V(coarseRows, coarseCols);

    Matrix2D newEps(0);
    if(epsilonMatrixGiven)
    {
        newEps = Matrix2D(coarseRows, coarseCols);
        restrict(newEps, epsMatrix);
    }

    /// recursie call
    basicCycle<W_CYCLE>(V, R, GridStep(2 * m_h), newEps, createSolver2DPtr(V, R, GridStep(2*m_h), newEps));
    //cout << rows << " " << cols << " " << m_h << endl;

    if(W_CYCLE)
    {
        basicCycle<W_CYCLE>(V, R, GridStep(2 * m_h), newEps, createSolver2DPtr(V, R, GridStep(2 * m_h), newEps));
        //cout << rows << " " << cols << " " << m_h << endl;
    }

    /// prolongatiion using interpolation
    Matrix2D v(rows + 2, cols + 2);
    prolongate(v, V);

    /// corretion of u
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            u(i, j) += v(i, j);
        }
    }

    /// post-smmothing steps
    for(Steps i{0}; i < solverSteps; ++i)
    {
        solverStrategy->solve();
    }
    solverStepsTotal += solverSteps;
}


void Poisson2D::solve()
{
    init();

    auto solver = createSolver2DPtr(phi, rho, m_h, epsilonMatrix);

    if(fullMultigridOn)
    {
        (this->*multigridFunction)(phi, rho, m_h, epsilonMatrix, solver);
        m_isSolved = true;
        return;
    }

    double residualOld = 0;
    double residual = solver->findSumResidual() + 2*residualDelta;
    SolverCalls calls{0};
    while(fabs(residual - residualOld) > residualDelta && calls < maxCalls)
    {
        (this->*multigridFunction)(phi, rho, m_h, epsilonMatrix, solver);
        residualOld = residual;
        residual = solver->findSumResidual();
        ++calls;
    }

    m_isSolved = true;
}

void Poisson2D::vCycle(Matrix2D& u, const Matrix2D& f, GridStep m_h, const Matrix2D& epsMatrix,
                       const Solver2DPtr& solverStrategy)
{
    basicCycle<false>(u, f, m_h, epsMatrix, solverStrategy);
}

void Poisson2D::wCycle(Matrix2D& u, const Matrix2D& f, GridStep m_h, const Matrix2D& epsMatrix,
                       const Solver2DPtr& solverStrategy)
{
    basicCycle<true>(u, f, m_h, epsMatrix, solverStrategy);
}

void Poisson2D::fCycle(Matrix2D& u, const Matrix2D& f, GridStep m_h, const Matrix2D& epsMatrix,
                       const Solver2DPtr& solverStrategy)
{
    // TODO temporary solution, should be added to basicCycle
    const size_t rows = u.rows() - 2;
    const size_t cols = u.cols() - 2;
    //cout << rows << " " << cols << " " << m_h << endl;

    /// do pre-smoothing Gauss-Seidel steps
    for(Steps i{0}; i < solverSteps; ++i)
    {
        solverStrategy->solve();
    }
    solverStepsTotal += solverSteps;

    /// solve exactly if there is only one interior point or number of points divisible by 2
    if(rows == 1 or cols == 1 or
       rows % 2 == 0 or cols % 2 == 0)
    {
        solverStrategy->solve();
        ++solverStepsTotal;
        return;
    }

    /// find the residual
    Matrix2D r = f;
    solverStrategy->findResidual(r);

    /// restricting to coarser grid
    size_t coarseRows = rows / 2 + 2;
    size_t coarseCols = cols / 2 + 2;
    Matrix2D R(coarseRows, coarseCols);
    restrict(R, r);

    /// initialize correction V on coarse grid
    Matrix2D V(coarseRows, coarseCols);

    Matrix2D newEps(0);
    if(epsilonMatrixGiven)
    {
        newEps = Matrix2D(coarseRows, coarseCols);
        restrict(newEps, epsMatrix);
    }

    /// recursie call
    fCycle(V, R, GridStep(2*m_h), newEps, createSolver2DPtr(V, R, GridStep(2*m_h), newEps));
    //cout << rows << " " << cols << " " << m_h << endl;
    basicCycle<false>(V, R, GridStep(2*m_h), newEps, createSolver2DPtr(V, R, GridStep(2*m_h), newEps));

    /// prolongatiion using interpolation
    Matrix2D v(rows + 2, cols + 2);
    prolongate(v, V);

    /// corretion of u
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            u(i, j) += v(i, j);
        }
    }

    /// post-smmothing steps
    for(Steps i{0}; i < solverSteps; ++i)
    {
        solverStrategy->solve();
    }
    solverStepsTotal += solverSteps;
}

void Poisson2D::fullMultigrid(Matrix2D& u, const Matrix2D& f, GridStep m_h, const Matrix2D& epsMatrix,
                              const Solver2DPtr& solverStrategy)
{
    const size_t rows = u.rows() - 2;
    const size_t cols = u.cols() - 2;

    if(rows == 1 or cols == 1 or
       rows % 2 == 0 or cols % 2 == 0)
    {
        solverStrategy->solve();
        ++solverStepsTotal;
    }
    else
    {
        size_t coarseRows = rows/2 + 2;
        size_t coarseCols = cols/2 + 2;
        Matrix2D F(coarseRows, coarseCols);
        restrictSimple(F, f);

        Matrix2D U(coarseRows, coarseCols);

        Matrix2D newEps(0);
        if(epsilonMatrixGiven)
        {
            newEps = Matrix2D(coarseRows, coarseCols);
            restrictSimple(newEps, epsMatrix);
        }

        fullMultigrid(U, F, GridStep(2*m_h), newEps, createSolver2DPtr(U, F, GridStep(2*m_h), newEps));

        prolongate(u, U);

        double residualOld = 0;
        double residual = solverStrategy->findSumResidual() + 2*residualDelta;
        SolverCalls calls{0};
        while(fabs(residual - residualOld) > residualDelta && calls < maxCalls)
        {
            fCycle(u, f, m_h, epsMatrix, solverStrategy);
            residualOld = residual;
            residual = solverStrategy->findSumResidual();
            ++calls;
        }
    }

}

void Poisson2D::setBoundaryValuesNorthOrSouth(const Array1D& values, size_t index)
{
    for(size_t i = 0; i < values.size(); ++i)
    {
        phi(index, i) = values[i];
    }
}

void Poisson2D::setBoundaryValuesWestOrEast(const Array1D& values, size_t index)
{
    for(size_t i = 0; i < values.size(); ++i)
    {
        phi(i, index) = values[i];
    }
}

void Poisson2D::setCondition(const Array1D& values, ConditionType2D condition)
{
    switch(condition)
    {
        case ConditionType2D::DirichletNorth: // [fallthrough]
        case ConditionType2D::NeumannNorth:
        {
            checkIfSizeMatchCols(values.size());
            setBoundaryValuesNorthOrSouth(values, m_nX+1);
            northCondition = condition;
            break;
        }
        case ConditionType2D::DirichletSouth: // [fallthrough]
        case ConditionType2D::NeumannSouth:
        {
            checkIfSizeMatchCols(values.size());
            setBoundaryValuesNorthOrSouth(values, 0);
            southCondition = condition;
            break;
        }
        case ConditionType2D::DirichletWest: // [fallthrough]
        case ConditionType2D::NeumannWest:
        {
            checkIfSizeMatchRows(values.size());
            setBoundaryValuesWestOrEast(values, 0);
            westCondition = condition;
            break;
        }
        case ConditionType2D::DirichletEast: // [fallthrough]
        case ConditionType2D::NeumannEast:
        {
            checkIfSizeMatchRows(values.size());
            setBoundaryValuesWestOrEast(values, m_nY+1);
            eastCondition = condition;
            break;
        }
    }
}

void Poisson2D::checkIfSizeMatchRows(size_t size)
{
    if(size != phi.rows())
    {
        throw std::logic_error("Condition array of size " + to_string(size) +
                               " doesn't match row size " + to_string(phi.rows()));
    }
}

void Poisson2D::checkIfSizeMatchCols(size_t size)
{
    if(size != phi.cols())
    {
        throw std::logic_error("Condition array of size " + to_string(size) +
                               " doesn't match column size " + to_string(phi.cols()));
    }
}

void Poisson2D::setMultigridCycle(MultigridCycle cycle)
{
    switch(cycle)
    {
        case MultigridCycle::VCycle:
        {
            fullMultigridOn = false;
            multigridFunction = &Poisson2D::vCycle;
            break;
        }
        case MultigridCycle::WCycle:
        {
            fullMultigridOn = false;
            multigridFunction = &Poisson2D::wCycle;
            break;
        }
        case MultigridCycle::FullMultigrid:
        {
            fullMultigridOn = true;
            multigridFunction = &Poisson2D::fullMultigrid;
            break;
        }
        case MultigridCycle::FCycle:
        {
            fullMultigridOn = false;
            multigridFunction = &Poisson2D::fCycle;
            break;
        }
    }
}

Solver2DPtr Poisson2D::createSolver2DPtr(Matrix2D& u, const Matrix2D& f, GridStep m_h, const Matrix2D& epsMatrix) noexcept
{
    return epsilonMatrixGiven ? SolverGSStrategy2DFactory::createSolverVarEps(northCondition, southCondition,
                                                                              westCondition, eastCondition,
                                                                              u, f, m_h, epsMatrix)
                              : SolverGSStrategy2DFactory::createSolverConstEps(northCondition, southCondition,
                                                                                westCondition, eastCondition,
                                                                                u, f, m_h, epsilonC.value());
}

#endif
}
