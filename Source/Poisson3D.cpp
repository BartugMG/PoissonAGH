#include "PoissonAGH/Poisson3D.hpp"
#include <iostream>

using namespace std;

namespace PoissonAGH
{
using namespace utils;
#ifdef WIP
void Poisson3D::init() const
{
    if(m_nX % 2 == 0 or m_nY % 2 == 0 or m_nZ % 2 == 0)
    {
        throw std::logic_error("Size of mesh must be odd number");
    }

    if(frontCondition == ConditionType3D::NeumannFront and backCondition == ConditionType3D::NeumannBack and
       upCondition == ConditionType3D::NeumannUp and downCondition == ConditionType3D::NeumannDown and
       leftCondition == ConditionType3D::NeumannLeft and rightCondition == ConditionType3D::NeumannRight)
    {
        throw std::logic_error("Neumann condtions can't be on every side!");
    }

    if(epsilonMatrixGiven and (rho.rows() != epsilonMatrix.rows() or
                               rho.cols() != epsilonMatrix.cols() or
                               rho.slices() != epsilonMatrix.slices()))
    {
        throw std::logic_error("Size of epsilon matrix not equal to rho matrix");
    }

    if(not (isPowerOf2(m_nX+1) and isPowerOf2(m_nY+1) and isPowerOf2((m_nZ+1))) )
    {
        cout << "Sizes of given inner mesh aren't 2^n-1, solution might not be optimal" << endl;
    }
}

void Poisson3D::solve()
{
    init();

    auto solver = createSolver3DPtr(phi, rho, m_h, epsilonMatrix);

    if(fullMultigridOn)
    {
        (this->*multigridFunction)(phi, rho, m_h, epsilonMatrix, solver);
        m_isSolved = true;
        return;
    }

    double residualOld = 0;
    double residual = solver->findSumResidual() + 2*residualVariability;
    SolverCalls calls{0};
    while(fabs(residual - residualOld) > residualVariability && calls < maxCalls)
    {
        (this->*multigridFunction)(phi, rho, m_h, epsilonMatrix, solver);
        residualOld = residual;
        residual = solver->findSumResidual();
        ++calls;
    }

    m_isSolved = true;
}

void Poisson3D::setMultigridCycle(MultigridCycle cycle)
{
    switch(cycle)
    {
        case MultigridCycle::VCycle:
        {
            fullMultigridOn = false;
            multigridFunction = &Poisson3D::vCycle;
            break;
        }
        case MultigridCycle::WCycle:
        {
            fullMultigridOn = false;
            multigridFunction = &Poisson3D::wCycle;
            break;
        }
        case MultigridCycle::FullMultigrid:
        {
            fullMultigridOn = true;
            multigridFunction = &Poisson3D::fullMultigrid;
            break;
        }
        case MultigridCycle::FCycle:
        {
            fullMultigridOn = false;
            multigridFunction = &Poisson3D::fCycle;
            break;
        }
    }
}

void Poisson3D::setCondition(const Matrix2D& values, ConditionType3D condition)
{
    switch(condition)
    {
        case ConditionType3D::DirichletFront: // [fallthrough]
        case ConditionType3D::NeumannFront:
        {
            checkIfSizeMatchCols(values.rows());
            checkIfSizeMatchSlices(values.cols());
            setBoundaryValuesFrontOrBack(values, 0);
            frontCondition = condition;
            break;
        }
        case ConditionType3D::DirichletBack: // [fallthrough]
        case ConditionType3D::NeumannBack:
        {
            checkIfSizeMatchCols(values.rows());
            checkIfSizeMatchSlices(values.cols());
            setBoundaryValuesFrontOrBack(values, m_nX+1);
            backCondition = condition;
            break;
        }
        case ConditionType3D::DirichletUp: // [fallthrough]
        case ConditionType3D::NeumannUp:
        {
            checkIfSizeMatchRows(values.rows());
            checkIfSizeMatchCols(values.cols());
            setBoundaryValuesUpOrDown(values, m_nZ+1);
            upCondition = condition;
            break;
        }
        case ConditionType3D::DirichletDown: // [fallthrough]
        case ConditionType3D::NeumannDown:
        {
            checkIfSizeMatchRows(values.rows());
            checkIfSizeMatchCols(values.cols());
            setBoundaryValuesUpOrDown(values, 0);
            downCondition = condition;
            break;
        }
        case ConditionType3D::DirichletLeft: // [fallthrough]
        case ConditionType3D::NeumannLeft:
        {
            checkIfSizeMatchRows(values.rows());
            checkIfSizeMatchSlices(values.cols());
            setBoundaryValuesLeftOrRight(values, 0);
            leftCondition = condition;
            break;
        }
        case ConditionType3D::DirichletRight: // [fallthrough]
        case ConditionType3D::NeumannRight:
        {
            checkIfSizeMatchRows(values.rows());
            checkIfSizeMatchSlices(values.cols());
            setBoundaryValuesLeftOrRight(values, m_nY+1);
            rightCondition = condition;
            break;
        }
    }

}

void Poisson3D::checkIfSizeMatchRows(size_t size) const
{
    if(size != phi.rows())
    {
        throw std::logic_error("Condition array of size " + to_string(size) +
                               " doesn't match row size " + to_string(phi.rows()));
    }
}

void Poisson3D::checkIfSizeMatchCols(size_t size) const
{
    if(size != phi.cols())
    {
        throw std::logic_error("Condition array of size " + to_string(size) +
                               " doesn't match column size " + to_string(phi.cols()));
    }
}

void Poisson3D::checkIfSizeMatchSlices(size_t size) const
{
    if(size != phi.slices())
    {
        throw std::logic_error("Condition array of size " + to_string(size) +
                               " doesn't match slice size " + to_string(phi.slices()));
    }
}

void Poisson3D::setBoundaryValuesFrontOrBack(const Matrix2D& values, size_t index)
{
    for(size_t i = 0; i < values.rows(); ++i)
    {
        for(size_t j = 0; j < values.cols(); ++j)
        {
            phi(index, i, j) = values(i, j);
        }
    }
}

void Poisson3D::setBoundaryValuesUpOrDown(const Matrix2D& values, size_t index)
{
    for(size_t i = 0; i < values.rows(); ++i)
    {
        for(size_t j = 0; j < values.cols(); ++j)
        {
            phi(i, j, index) = values(i, j);
        }
    }
}

void Poisson3D::setBoundaryValuesLeftOrRight(const Matrix2D& values, size_t index)
{
    for(size_t i = 0; i < values.rows(); ++i)
    {
        for(size_t j = 0; j < values.cols(); ++j)
        {
            phi(i, index, j) = values(i, j);
        }
    }
}

void Poisson3D::prolongate(Matrix3D& v, const Matrix3D& V)
{
    const size_t rows = V.rows() - 2;
    const size_t cols = V.cols() - 2;
    const size_t slices = V.slices() - 2;

    for(size_t I = 1; I <= rows; ++I)
    {
        for(size_t J = 1; J <= cols; ++J)
        {
            for(size_t K = 1; K <= slices; ++K)
            {
                v(2*I, 2*J, 2*K) = V(I, J, K);

                interpolateHalfs(v, V, I, J, K);
                interpolateQuaters(v, V, I, J, K);
                interpolateEighths(v, V, I, J, K);

            }
        }
    }

}

void Poisson3D::interpolateHalfs(Matrix3D& v, const Matrix3D& V, size_t I, size_t J, size_t K)
{
    const size_t i = 2*I;
    const size_t j = 2*J;
    const size_t k = 2*K;

    v(i-1, j, k) = 0.5 * (V(I, J, K) + V(I-1, J, K));
    v(i+1, j, k) = 0.5 * (V(I, J, K) + V(I+1, J, K));
    v(i, j-1, k) = 0.5 * (V(I, J, K) + V(I, J-1, K));
    v(i, j+1, k) = 0.5 * (V(I, J, K) + V(I, J+1, K));
    v(i, j, k-1) = 0.5 * (V(I, J, K) + V(I, J, K-1));
    v(i, j, k+1) = 0.5 * (V(I, J, K) + V(I, J, K+1));
}

void Poisson3D::interpolateQuaters(Matrix3D& v, const Matrix3D& V, size_t I, size_t J, size_t K)
{
    const size_t i = 2*I;
    const size_t j = 2*J;
    const size_t k = 2*K;

    v(i-1, j-1, k) = 0.25 * (V(I, J, K) + V(I-1, J, K) + V(I, J-1, K) + V(I-1, J-1, K));
    v(i+1, j-1, k) = 0.25 * (V(I, J, K) + V(I+1, J, K) + V(I, J-1, K) + V(I+1, J-1, K));
    v(i-1, j+1, k) = 0.25 * (V(I, J, K) + V(I-1, J, K) + V(I, J+1, K) + V(I-1, J+1, K));
    v(i+1, j+1, k) = 0.25 * (V(I, J, K) + V(I+1, J, K) + V(I, J+1, K) + V(I+1, J+1, K));

    v(i-1, j, k-1) = 0.25 * (V(I, J, K) + V(I-1, J, K) + V(I, J, K-1) + V(I-1, J, K-1));
    v(i+1, j, k-1) = 0.25 * (V(I, J, K) + V(I+1, J, K) + V(I, J, K-1) + V(I+1, J, K-1));
    v(i-1, j, k+1) = 0.25 * (V(I, J, K) + V(I-1, J, K) + V(I, J, K+1) + V(I-1, J, K+1));
    v(i+1, j, k+1) = 0.25 * (V(I, J, K) + V(I+1, J, K) + V(I, J, K+1) + V(I+1, J, K+1));

    v(i, j-1, k-1) = 0.25 * (V(I, J, K) + V(I, J-1, K) + V(I, J, K-1) + V(I, J-1, K-1));
    v(i, j+1, k-1) = 0.25 * (V(I, J, K) + V(I, J+1, K) + V(I, J, K-1) + V(I, J+1, K-1));
    v(i, j-1, k+1) = 0.25 * (V(I, J, K) + V(I, J-1, K) + V(I, J, K+1) + V(I, J-1, K+1));
    v(i, j+1, k+1) = 0.25 * (V(I, J, K) + V(I, J+1, K) + V(I, J, K+1) + V(I, J+1, K+1));
}

void Poisson3D::interpolateEighths(Matrix3D& v, const Matrix3D& V, size_t I, size_t J, size_t K)
{
    const size_t i = 2*I;
    const size_t j = 2*J;
    const size_t k = 2*K;

    v(i-1, j-1, k-1) = 0.125 * (V(I, J, K) + V(I-1, J, K) + V(I, J-1, K) + V(I, J, K-1) +
                                V(I-1, J-1, K) + V(I-1, J, K-1) + V(I, J-1, K-1) + V(I-1, J-1, K-1));
    v(i+1, j-1, k-1) = 0.125 * (V(I, J, K) + V(I+1, J, K) + V(I, J-1, K) + V(I, J, K-1) +
                                V(I+1, J-1, K) + V(I+1, J, K-1) + V(I, J-1, K-1) + V(I+1, J-1, K-1));
    v(i-1, j+1, k-1) = 0.125 * (V(I, J, K) + V(I-1, J, K) + V(I, J+1, K) + V(I, J, K-1) +
                                V(I-1, J+1, K) + V(I-1, J, K-1) + V(I, J+1, K-1) + V(I-1, J+1, K-1));
    v(i-1, j-1, k+1) = 0.125 * (V(I, J, K) + V(I-1, J, K) + V(I, J-1, K) + V(I, J, K+1) +
                                V(I-1, J-1, K) + V(I-1, J, K+1) + V(I, J-1, K+1) + V(I-1, J-1, K+1));
    v(i+1, j+1, k-1) = 0.125 * (V(I, J, K) + V(I+1, J, K) + V(I, J+1, K) + V(I, J, K-1) +
                                V(I+1, J+1, K) + V(I+1, J, K-1) + V(I, J+1, K-1) + V(I+1, J+1, K-1));
    v(i-1, j+1, k+1) = 0.125 * (V(I, J, K) + V(I-1, J, K) + V(I, J+1, K) + V(I, J, K+1) +
                                V(I-1, J+1, K) + V(I-1, J, K+1) + V(I, J+1, K+1) + V(I-1, J+1, K+1));
    v(i+1, j-1, k+1) = 0.125 * (V(I, J, K) + V(I+1, J, K) + V(I, J-1, K) + V(I, J, K+1) +
                                V(I+1, J-1, K) + V(I+1, J, K+1) + V(I, J-1, K+1) + V(I+1, J-1, K+1));
    v(i+1, j+1, k+1) = 0.125 * (V(I, J, K) + V(I+1, J, K) + V(I, J+1, K) + V(I, J, K+1) +
                                V(I+1, J+1, K) + V(I+1, J, K+1) + V(I, J+1, K+1) + V(I+1, J+1, K+1));
}

void Poisson3D::restrict(Matrix3D& R, const Matrix3D& r)
{
    const size_t rows = R.rows() - 2;
    const size_t cols = R.cols() - 2;
    const size_t slices = R.slices() - 2;

    for(size_t I = 1; I <= rows; ++I)
    {
        const size_t i = 2*I;
        for(size_t J = 1; J <= cols; ++J)
        {
            const size_t j = 2*J;
            for(size_t K = 1; K <= slices; ++K)
            {
                const size_t k = 2*K;
                R(I, J, K) = (1./64.) * (r(i-1, j-1, k-1) + r(i+1, j-1, k-1) + r(i-1, j+1, k-1) + r(i-1, j-1, k+1) +
                                         r(i+1, j+1, k-1) + r(i+1, j-1, k+1) + r(i-1, j+1, k+1) + r(i+1, j+1, k+1) +
                                         2 * (r(i-1, j-1, k) + r(i+1, j-1, k) + r(i-1, j+1, k) + r(i+1, j+1, k) +
                                              r(i-1, j, k-1) + r(i+1, j, k-1) + r(i-1, j, k+1) + r(i+1, j, k+1) +
                                              r(i, j-1, k-1) + r(i, j+1, k-1) + r(i, j-1, k+1) + r(i, j+1, k+1)) +
                                         4 * (r(i-1, j, k) + r(i+1, j, k) + r(i, j-1, k) + r(i, j+1, k) +
                                              r(i, j, k-1) + r(i, j, k+1)) + 8*r(i, j, k) );
            }
        }
    }
}

void Poisson3D::restrictSimple(Matrix3D& R, const Matrix3D& r)
{
    const size_t rows = R.rows() - 2;
    const size_t cols = R.cols() - 2;
    const size_t slices = R.slices() - 2;

    for(size_t I = 1; I <= rows; ++I)
    {
        const size_t i = 2*I;
        for(size_t J = 1; J <= cols; ++J)
        {
            const size_t j = 2*J;
            for(size_t K = 1; K <= slices; ++K)
            {
                const size_t k = 2*K;
                R(I, J, K) = r(i, j, k);
            }
        }
    }

    restrictBorders(R, r);
}

void Poisson3D::restrictBorders(Matrix3D& R, const Matrix3D& r)
{
    for(size_t I = 0; I < R.rows(); ++I)
    {
        const size_t i = 2*I;
        for(size_t J = 0; J < R.cols(); ++J)
        {
            const size_t j = 2*J;
            R(I, J, 0) = r(i, j, 0);
            R(I, J, R.slices()-1) = r(i, j, r.slices()-1);
        }

        for(size_t K = 0; K < R.slices(); ++K)
        {
            const size_t k = 2*K;
            R(I, 0, K) = r(i, 0, k);
            R(I, R.cols()-1, K) = r(i, r.cols()-1, k);
        }
    }

    for(size_t J = 0; J < R.cols(); ++J)
    {
        const size_t j = 2*J;
        for(size_t K = 0; K < R.slices(); ++K)
        {
            const size_t k = 2*K;
            R(0, J, K) = r(0, j, k);
            R(R.rows()-1, J, K) = r(r.rows()-1, j, k);
        }
    }
}

template<bool W_CYCLE>
void Poisson3D::basicCycle(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& epsMatrix, const Solver3DPtr& solverStrategy)
{
    const size_t rows = u.rows() - 2;
    const size_t cols = u.cols() - 2;
    const size_t slices = u.slices() - 2;
    //cout << rows << " " << cols << " " << slices << " " << h << endl;

    /// do pre-smoothing Gauss-Seidel steps
    for(Steps i{0}; i < solverSteps; ++i)
    {
        solverStrategy->solve();
    }
    solverStepsTotal += solverSteps;

    /// solve exactly if there is only one interior point or number of points divisible by 2
    if(rows == 1 or cols == 1 or slices == 1 or
       rows % 2 == 0 or cols % 2 == 0 or slices % 2 == 0)
    {
        solverStrategy->solve();
        ++solverStepsTotal;
        return;
    }

    /// find the residual
    Matrix3D r = f;
    solverStrategy->findResidual(r);

    /// restricting to coarser grid
    size_t coarseRows = rows / 2 + 2;
    size_t coarseCols = cols / 2 + 2;
    size_t coarseSlices = slices / 2 + 2;
    Matrix3D R(coarseRows, coarseCols, coarseSlices);
    restrict(R, r);

    /// initialize correction V on coarse grid
    Matrix3D V(coarseRows, coarseCols, coarseSlices);

    Matrix3D newEps(0);
    if(epsilonMatrixGiven)
    {
        newEps = Matrix3D(coarseRows, coarseCols, coarseSlices);
        restrict(newEps, epsMatrix);
    }

    /// recursie call
    basicCycle<W_CYCLE>(V, R, 2*h, newEps, createSolver3DPtr(V, R, 2 * h, newEps));

    if(W_CYCLE)
    {
        basicCycle<W_CYCLE>(V, R, 2*h, newEps, createSolver3DPtr(V, R, 2 * h, newEps));
    }

    /// prolongatiion using interpolation
    Matrix3D v(rows + 2, cols + 2, slices + 2);
    prolongate(v, V);

    /// corretion of u
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            for(size_t k = 1; k <= slices; ++k)
            {
                u(i, j, k) += v(i, j, k);
            }
        }
    }

    /// post-smmothing steps
    for(Steps i{0}; i < solverSteps; ++i)
    {
        solverStrategy->solve();
    }
    solverStepsTotal += solverSteps;
}

void Poisson3D::fCycle(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& epsMatrix,
                       const Solver3DPtr& solverStrategy)
{
    const size_t rows = u.rows() - 2;
    const size_t cols = u.cols() - 2;
    const size_t slices = u.slices() - 2;
    //cout << rows << " " << cols << " " << slices << " " << h << endl;

    /// do pre-smoothing Gauss-Seidel steps
    for(Steps i{0}; i < solverSteps; ++i)
    {
        solverStrategy->solve();
    }
    solverStepsTotal += solverSteps;

    /// solve exactly if there is only one interior point or number of points divisible by 2
    if(rows == 1 or cols == 1 or slices == 1 or
       rows % 2 == 0 or cols % 2 == 0 or slices % 2 == 0)
    {
        solverStrategy->solve();
        ++solverStepsTotal;
        return;
    }

    /// find the residual
    Matrix3D r = f;
    solverStrategy->findResidual(r);

    /// restricting to coarser grid
    size_t coarseRows = rows / 2 + 2;
    size_t coarseCols = cols / 2 + 2;
    size_t coarseSlices = slices / 2 + 2;
    Matrix3D R(coarseRows, coarseCols, coarseSlices);
    restrict(R, r);

    /// initialize correction V on coarse grid
    Matrix3D V(coarseRows, coarseCols, coarseSlices);

    Matrix3D newEps(0);
    if(epsilonMatrixGiven)
    {
        newEps = Matrix3D(coarseRows, coarseCols, coarseSlices);
        restrict(newEps, epsMatrix);
    }

    /// recursie call
    fCycle(V, R, 2*h, newEps, createSolver3DPtr(V, R, 2 * h, newEps));
    basicCycle<false>(V, R, 2*h, newEps, createSolver3DPtr(V, R, 2 * h, newEps));

    /// prolongatiion using interpolation
    Matrix3D v(rows + 2, cols + 2, slices + 2);
    prolongate(v, V);

    /// corretion of u
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            for(size_t k = 1; k <= slices; ++k)
            {
                u(i, j, k) += v(i, j, k);
            }
        }
    }

    /// post-smmothing steps
    for(Steps i{0}; i < solverSteps; ++i)
    {
        solverStrategy->solve();
    }
    solverStepsTotal += solverSteps;
}

void Poisson3D::vCycle(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& epsMatrix,
                       const Solver3DPtr& solverStrategy)
{
    basicCycle<false>(u, f, h, epsMatrix, solverStrategy);
}

void Poisson3D::wCycle(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& epsMatrix,
                       const Solver3DPtr& solverStrategy)
{
    basicCycle<true>(u, f, h, epsMatrix, solverStrategy);
}

void Poisson3D::fullMultigrid(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& epsMatrix,
                              const Solver3DPtr& solverStrategy)
{
    const size_t rows = u.rows() - 2;
    const size_t cols = u.cols() - 2;
    const size_t slices = u.slices() - 2;

    if(rows == 1 or cols == 1 or slices == 1 or
       rows % 2 == 0 or cols % 2 == 0 or slices % 2 == 0)
    {
        solverStrategy->solve();
        ++solverStepsTotal;
    }
    else
    {
        size_t coarseRows = rows/2 + 2;
        size_t coarseCols = cols/2 + 2;
        size_t coarseSlices = slices/2 + 2;
        Matrix3D F(coarseRows, coarseCols, coarseSlices);
        restrictSimple(F, f);

        Matrix3D U(coarseRows, coarseCols, coarseSlices);

        Matrix3D newEps(0);
        if(epsilonMatrixGiven)
        {
            newEps = Matrix3D(coarseRows, coarseCols, coarseSlices);
            restrictSimple(newEps, epsMatrix);
        }

        fullMultigrid(U, F, 2*h, newEps, createSolver3DPtr(U, F, 2*h, newEps));

        prolongate(u, U);

        double residualOld = 0;
        double residual = solverStrategy->findSumResidual() + 2*residualVariability;
        SolverCalls calls{0};
        while(fabs(residual - residualOld) > residualVariability && calls < maxCalls)
        {
            fCycle(u, f, h, epsMatrix, solverStrategy);
            residualOld = residual;
            residual = solverStrategy->findSumResidual();
            ++calls;
        }
    }
}

Solver3DPtr Poisson3D::createSolver3DPtr(Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& epsMatrix) noexcept
{
    return epsilonMatrixGiven ? SolverGSStrategy3DFactory::createSolverVarEps(frontCondition, backCondition, upCondition,
                                                                              downCondition, leftCondition, rightCondition,
                                                                              u, f, h, epsMatrix)
                              : SolverGSStrategy3DFactory::createSolverConstEps(frontCondition, backCondition, upCondition,
                                                                                downCondition, leftCondition, rightCondition,
                                                                                u, f, h, epsilonC);
}

#endif
}
