include_directories(${PROJECT_SOURCE_DIR}/Include)

add_library(poisson SHARED Poisson2D.cpp Poisson3D.cpp
        SolverStrategy2D.cpp SolverStrategy3D.cpp PTools.cpp
        ../Include/PoissonAGH/Utils/PTypes.hpp
        ../Include/PoissonAGH/Utils/PStrongType.hpp
        ../Include/PoissonAGH/Utils/PMatrix.hpp
        ../Include/PoissonAGH/Poisson1D.hpp
        ../Include/PoissonAGH/Solver/PSolver1D.hpp PSolver1D.cpp
        ../Include/PoissonAGH/Meta/PTypeMap1D.hpp)

add_library(poissonC_API SHARED Poisson1D_C.cpp Poisson2D_C.cpp Poisson3D_C.cpp)
target_link_libraries(poissonC_API poisson)

set_target_properties(poisson PROPERTIES
        LIBRARY_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/build/lib
        )

set_target_properties(poissonC_API PROPERTIES
        LIBRARY_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/build/lib
        )

