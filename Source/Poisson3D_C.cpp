#include "PoissonAGH/Poisson3D_C.h"
#include "PoissonAGH/Poisson3D.hpp"

using namespace PoissonAGH;

//auto P3D_reinterpret = [](Poisson3D_Ptr ptr){ return reinterpret_cast<Poisson3D*>(ptr); };

#ifdef __cplusplus
extern "C" {
#endif

# ifdef WIP
Poisson3D_Ptr P3D_new_Poisson3D_cubeStep(double xmin, double xmax, double h)
{
    return reinterpret_cast<Poisson3D_Ptr>(new Poisson3D(xmin, xmax, h));
}

Poisson3D_Ptr P3D_new_Poisson3D_cubeSize(double xmin, double xmax, size_t nx)
{
    return reinterpret_cast<Poisson3D_Ptr>(new Poisson3D(xmin, xmax, nx));
}

Poisson3D_Ptr P3D_new_Poisson3D_cuboidalStep(double xmin, double xmax, double ymin, double ymax,
                                             double zmin, double zmax, double h)
{
    return reinterpret_cast<Poisson3D_Ptr>(new Poisson3D(xmin, xmax, ymin, ymax, zmin, zmax, h));
}

Poisson3D_Ptr P3D_new_Poisson3D_matrix(double*** rho_arr, size_t rows, size_t cols, size_t slices, double h)
{
    Matrix3D data(rho_arr, rows, cols, slices);
    return reinterpret_cast<Poisson3D_Ptr>(new Poisson3D(data, h));
}

Poisson3D_Ptr P3D_new_Poisson3D_array(double* rho_arr, size_t rows, size_t cols, size_t slices, double h)
{
    Matrix3D data(rho_arr, rows, cols, slices);
    return reinterpret_cast<Poisson3D_Ptr>(new Poisson3D(data, h));
}

void P3D_delete_Poisson3D(Poisson3D_Ptr solver)
{
    delete P3D_reinterpret(solver);
}

int P3D_isSolved(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->isSolved();
}

double P3D_getXmin(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->getXmin();
}

double P3D_getXmax(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->getXmax();
}

double P3D_getYmin(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->getYmin();
}

double P3D_getYmax(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->getYmax();
}

double P3D_getZmin(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->getZmin();
}

double P3D_getZmax(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->getZmax();
}

size_t P3D_getNx(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->getNx();
}
size_t P3D_getNy(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->getNy();
}

size_t P3D_getNz(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->getNz();
}

double P3D_getStep(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->getStep();
}

double P3D_getEpsilonC(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->epsilonConstant;
}

void P3D_setEpsilonC(Poisson3D_Ptr solver, double epsilonC)
{
    P3D_reinterpret(solver)->epsilonConstant = epsilonC;
}

const double* P3D_getEpsilonMatrix(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->epsilonMatrix.getData();
}

void P3D_setEpsilonMatrix(Poisson3D_Ptr solver, const double* eps_matrix)
{
    Poisson3D* ptr = P3D_reinterpret(solver);
    Matrix3D epsilon(eps_matrix, ptr->getNx()+2, ptr->getNy()+2, ptr->getNz()+2);
    ptr->setEpsilonMatrix(std::move(epsilon));
}

const double* P3D_getPhiMatrix(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->phi.getData();
}

const double* P3D_getRhoMatrix(Poisson3D_Ptr solver)
{
    return P3D_reinterpret(solver)->rho.getData();
}

void P3D_setSolverSteps(Poisson3D_Ptr solver, size_t steps)
{
    P3D_reinterpret(solver)->solverSteps = steps;
}

void P3D_setMaxMultigridCalls(Poisson3D_Ptr solver, size_t calls)
{
    P3D_reinterpret(solver)->maxCalls = calls;
}

void P3D_setResidualDelta(Poisson3D_Ptr solver, double delta)
{
    P3D_reinterpret(solver)->residualVariability = delta;
}

void P3D_setCondition(Poisson3D_Ptr solver, const double* values, size_t lenX, size_t lenY, P3D_ConditionType3D condition)
{
    Matrix2D data(values, lenX, lenY);
    P3D_reinterpret(solver)->setCondition(data, static_cast<ConditionType3D>(condition));
}

P3D_ConditionType3D P3D_getFrontCondition(Poisson3D_Ptr solver)
{
    return static_cast<P3D_ConditionType3D>(P3D_reinterpret(solver)->getFrontCondition());
}

P3D_ConditionType3D P3D_getBackCondition(Poisson3D_Ptr solver)
{
    return static_cast<P3D_ConditionType3D>(P3D_reinterpret(solver)->getBackCondition());
}

P3D_ConditionType3D P3D_getUpCondition(Poisson3D_Ptr solver)
{
    return static_cast<P3D_ConditionType3D>(P3D_reinterpret(solver)->getUpCondition());
}

P3D_ConditionType3D P3D_getDownCondition(Poisson3D_Ptr solver)
{
    return static_cast<P3D_ConditionType3D>(P3D_reinterpret(solver)->getDownCondition());
}

P3D_ConditionType3D P3D_getLeftCondition(Poisson3D_Ptr solver)
{
    return static_cast<P3D_ConditionType3D>(P3D_reinterpret(solver)->getLeftCondition());
}

P3D_ConditionType3D P3D_getRightCondition(Poisson3D_Ptr solver)
{
    return static_cast<P3D_ConditionType3D>(P3D_reinterpret(solver)->getRightCondition());
}

void P3D_setMultigridType(Poisson3D_Ptr solver, P3D_MultigridType multigridType)
{
    P3D_reinterpret(solver)->setMultigridCycle(static_cast<MultigridCycle>(multigridType));
}

void P3D_solve(Poisson3D_Ptr solver)
{
    P3D_reinterpret(solver)->solve();
}

double P3D_getValueAt(const double* matrix, size_t columns, size_t slices, size_t i, size_t j, size_t k)
{
    return matrix[k + j*slices + i*columns*slices];
}

void P3D_setValueAt(double* matrix, double value, size_t columns, size_t slices, size_t i, size_t j, size_t k)
{
    matrix[k + j*slices + i*columns*slices] = value;
}
#endif

#ifdef __cplusplus
}
#endif
