#include "PoissonAGH/Solver/SolverStrategy3D.hpp"

namespace PoissonAGH
{
namespace utils
{
using namespace std;

inline void AllDirichletGSConstEps3D::gaussSeidelOneColour(IGaussSeidelSolverStrategy3D::Colour colour) noexcept
{
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            for(size_t k = 2 - (i + j + colour) % 2; k <= slices; k += 2)
            {
                u(i, j, k) = (1./6.) * (u(i-1, j, k) + u(i+1, j, k) + u(i, j-1, k) + u(i, j+1, k) +
                                        u(i, j, k-1) + u(i, j, k+1) + h*h*f(i, j, k)/eps);
            }
        }
    }
}

void AllDirichletGSConstEps3D::solve() noexcept
{
    gaussSeidelOneColour(Colour::RED);
    gaussSeidelOneColour(Colour::BLACK);
}

void AllDirichletGSConstEps3D::findResidual(Matrix3D& r) noexcept
{
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            for(size_t k = 1; k <= slices; ++k)
            {
                r(i, j, k) += eps*(u(i+1, j, k) + u(i-1, j, k) + u(i, j-1, k) + u(i, j+1, k) + u(i, j, k-1) +
                                   u(i, j, k+1) - 6*u(i, j, k)) / (h*h);
            }
        }
    }
}

double AllDirichletGSConstEps3D::findSumResidual() noexcept
{
    double sum = 0;

    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            for(size_t k = 1; k <= slices; ++k)
            {
                sum += f(i, j, k) + (eps*(u(i+1, j, k) + u(i-1, j, k) + u(i, j-1, k) + u(i, j+1, k) + u(i, j, k-1) +
                                     u(i, j, k+1) - 6*u(i, j, k)) / (h*h));
            }
        }
    }
    return sum;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

NeumannSolverGSConstEps3D::NeumannSolverGSConstEps3D(bool frontNeumann, bool backNeumann, bool upNeumann,
                                                     bool downNeumann, bool leftNeumann, bool rightNeumann, Matrix3D& u,
                                                     const Matrix3D& f, double h, double eps):
        u(u), f(f), h(h), eps(eps), rows(u.rows()-2), cols(u.cols()-2), slices(u.slices()-2)
{
    hMinDown = downNeumann ? -2*h : 1.;
    hMaxDown = downNeumann ? 2 : 1.;
    hMinUp = upNeumann ? 2 : 1.;
    hMaxUp = upNeumann ? 2*h : 1.;
    hMinLeft = leftNeumann ? -2*h : 1.;
    hMaxLeft = leftNeumann ? 2 : 1.;
    hMinRight = rightNeumann ? 2 : 1.;
    hMaxRight = rightNeumann ? 2*h : 1.;
    hMinFront = frontNeumann ? -2*h : 1.;
    hMaxFront = frontNeumann ? 2 : 1.;
    hMinBack = backNeumann ? 2 : 1.;
    hMaxBack = backNeumann ? 2*h : 1.;
}

inline void NeumannSolverGSConstEps3D::solveDirichlet(Matrix3D& u, size_t i, size_t j, size_t k) const noexcept
{
    u(i, j, k) = (u(i-1, j, k) + u(i+1, j, k) + u(i, j-1, k) + u(i, j+1, k) + u(i, j, k-1) + u(i, j, k+1)
                  + h*h*f(i, j, k)/eps) / 6.;
}

inline void NeumannSolverGSConstEps3D::solveNeumann(Matrix3D& u, size_t i, size_t j, size_t k, double hXmin, double hXmax,
                                                    double hYmin, double hYmax, double hZmin, double hZmax) const noexcept
{
    u(i, j, k) = (hXmin*u(i-1, j, k) + hXmax*u(i+1, j, k) + hYmin*u(i, j-1, k) + hYmax*u(i, j+1, k)
                  + hZmin*u(i, j, k-1) + hZmax*u(i, j, k+1) + h*h*f(i, j, k)/eps) / 6.;
}

void NeumannSolverGSConstEps3D::gaussSeidelOneColour(Colour colour) noexcept
{
    generalSolverTemplate(&NeumannSolverGSConstEps3D::solveNeumann, &NeumannSolverGSConstEps3D::solveDirichlet, u, colour);
}

void NeumannSolverGSConstEps3D::solve() noexcept
{
    gaussSeidelOneColour(Colour::RED);
    gaussSeidelOneColour(Colour::BLACK);
}

inline void NeumannSolverGSConstEps3D::findResidualDirichlet(Matrix3D& r, size_t i, size_t j, size_t k) const noexcept
{
    r(i, j, k) += eps*(u(i+1, j, k) + u(i-1, j, k) + u(i, j-1, k) + u(i, j+1, k) + u(i, j, k-1)
                       + u(i, j, k+1) - 6*u(i, j, k)) / (h*h);
}

inline void NeumannSolverGSConstEps3D::findResidualNeumann(Matrix3D& r, size_t i, size_t j, size_t k,
                                                    double hXmin, double hXmax, double hYmin, double hYmax, double hZmin,
                                                    double hZmax) const noexcept
{
    r(i, j, k) += eps*(hXmax*u(i+1, j, k) + hXmin*u(i-1, j, k) + hYmin*u(i, j-1, k) + hYmax*u(i, j+1, k) + hZmin*u(i, j, k-1)
                       + hZmax*u(i, j, k+1) - 6*u(i, j, k)) / (h*h);
}

void NeumannSolverGSConstEps3D::findResidual(Matrix3D& r) noexcept
{
    generalSolverTemplate(&NeumannSolverGSConstEps3D::findResidualNeumann,
                          &NeumannSolverGSConstEps3D::findResidualDirichlet, r, Colour::RED);
    generalSolverTemplate(&NeumannSolverGSConstEps3D::findResidualNeumann,
                          &NeumannSolverGSConstEps3D::findResidualDirichlet, r, Colour::BLACK);
}

inline void NeumannSolverGSConstEps3D::findSumResidualDirichlet(double& sum, size_t i, size_t j, size_t k) const noexcept
{
    sum += f(i, j, k) + (eps*(u(i+1, j, k) + u(i-1, j, k) + u(i, j-1, k) + u(i, j+1, k) + u(i, j, k-1)
                         + u(i, j, k+1) - 6*u(i, j, k)) / (h*h));
}

inline void NeumannSolverGSConstEps3D::findSumResidualNeumann(double& sum, size_t i, size_t j, size_t k, double hXmin, double hXmax, double hYmin,
                                                       double hYmax, double hZmin, double hZmax) const noexcept
{
    sum += f(i, j, k) + (eps*(hXmax*u(i+1, j, k) + hXmin*u(i-1, j, k) + hYmin*u(i, j-1, k) + hYmax*u(i, j+1, k) + hZmin*u(i, j, k-1)
                        + hZmax*u(i, j, k+1) - 6*u(i, j, k)) / (h*h));
}

double NeumannSolverGSConstEps3D::findSumResidual() noexcept
{
    double sum = 0;
    generalSolverTemplate(&NeumannSolverGSConstEps3D::findSumResidualNeumann,
                          &NeumannSolverGSConstEps3D::findSumResidualDirichlet, sum, Colour::RED);
    generalSolverTemplate(&NeumannSolverGSConstEps3D::findSumResidualNeumann,
                          &NeumannSolverGSConstEps3D::findSumResidualDirichlet, sum, Colour::BLACK);
    return sum;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void AllDirichletGSVarEps3D::gaussSeidelOneColour(Colour colour) noexcept
{
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            for(size_t k = 2 - (i + j + colour) % 2; k <= slices; k += 2)
            {
                const double epsNI = eps(i+1, j, k) + eps(i, j, k);
                const double epsPI = eps(i-1, j, k) + eps(i, j, k);
                const double epsNJ = eps(i, j+1, k) + eps(i, j, k);
                const double epsPJ = eps(i, j-1, k) + eps(i, j, k);
                const double epsNK = eps(i, j, k+1) + eps(i, j, k);
                const double epsPK = eps(i, j, k-1) + eps(i, j, k);

                u(i, j, k) = (epsPI*u(i-1, j, k) + epsNI*u(i+1, j, k) + epsPJ*u(i, j-1, k) + epsNJ*u(i, j+1, k) +
                              epsPK*u(i, j, k-1) + epsNK*u(i, j, k+1) + 2*h*h*f(i, j, k)) /
                             (epsNI + epsPI + epsNJ + epsPJ + epsNK + epsPK);
            }
        }
    }
}

void AllDirichletGSVarEps3D::solve() noexcept
{
    gaussSeidelOneColour(Colour::RED);
    gaussSeidelOneColour(Colour::BLACK);
}

void AllDirichletGSVarEps3D::findResidual(Matrix3D& r) noexcept
{
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            for(size_t k = 1; k <= slices; ++k)
            {
                const double epsNI = eps(i+1, j, k) + eps(i, j, k);
                const double epsPI = eps(i-1, j, k) + eps(i, j, k);
                const double epsNJ = eps(i, j+1, k) + eps(i, j, k);
                const double epsPJ = eps(i, j-1, k) + eps(i, j, k);
                const double epsNK = eps(i, j, k+1) + eps(i, j, k);
                const double epsPK = eps(i, j, k-1) + eps(i, j, k);

                r(i, j, k) += (epsPI*u(i-1, j, k) + epsNI*u(i+1, j, k) + epsPJ*u(i, j-1, k) + epsNJ*u(i, j+1, k) +
                               epsPK*u(i, j, k-1) + epsNK*u(i, j, k+1) -
                              (epsNI + epsPI + epsNJ + epsPJ + epsNK + epsPK)*u(i, j, k)) / (2*h*h);
            }
        }
    }
}

double AllDirichletGSVarEps3D::findSumResidual() noexcept
{
    double sum = 0;
    for(size_t i = 1; i <= rows; ++i)
    {
        for(size_t j = 1; j <= cols; ++j)
        {
            for(size_t k = 1; k <= slices; ++k)
            {
                const double epsNI = eps(i+1, j, k) + eps(i, j, k);
                const double epsPI = eps(i-1, j, k) + eps(i, j, k);
                const double epsNJ = eps(i, j+1, k) + eps(i, j, k);
                const double epsPJ = eps(i, j-1, k) + eps(i, j, k);
                const double epsNK = eps(i, j, k+1) + eps(i, j, k);
                const double epsPK = eps(i, j, k-1) + eps(i, j, k);

                sum += f(i, j, k) + (epsPI*u(i-1, j, k) + epsNI*u(i+1, j, k) + epsPJ*u(i, j-1, k) + epsNJ*u(i, j+1, k) +
                                     epsPK*u(i, j, k-1) + epsNK*u(i, j, k+1) -
                                    (epsNI + epsPI + epsNJ + epsPJ + epsNK + epsPK)*u(i, j, k)) / (2*h*h);
            }
        }
    }
    return sum;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

NeumannSolverGSVarEps3D::NeumannSolverGSVarEps3D(bool isFront, bool isBack, bool isUp, bool isDown, bool isLeft,
                                                 bool isRight, Matrix3D& u, const Matrix3D& f, double h,
                                                 const Matrix3D& eps):
        u(u), f(f), h(h), eps(eps), rows(u.rows()-2), cols(u.cols()-2), slices(u.slices()-2),
        frontDirichlet(&NeumannSolverGSVarEps3D::createPartialDirichletSolver<-1, 0, 0>),
        backDirichlet(&NeumannSolverGSVarEps3D::createPartialDirichletSolver<1, 0, 0>),
        leftDirichlet(&NeumannSolverGSVarEps3D::createPartialDirichletSolver<0, -1, 0>),
        rightDirichlet(&NeumannSolverGSVarEps3D::createPartialDirichletSolver<0, 1, 0>),
        downDirichlet(&NeumannSolverGSVarEps3D::createPartialDirichletSolver<0, 0, -1>),
        upDirichlet(&NeumannSolverGSVarEps3D::createPartialDirichletSolver<0, 0, 1>)
{
    frontNeumann = isFront ? &NeumannSolverGSVarEps3D::createPartialNeumannSolver<-1, 0, 0> : frontDirichlet;
    backNeumann  = isBack  ? &NeumannSolverGSVarEps3D::createPartialNeumannSolver<1, 0, 0>  : backDirichlet;
    leftNeumann  = isLeft  ? &NeumannSolverGSVarEps3D::createPartialNeumannSolver<0, -1, 0> : leftDirichlet;
    rightNeumann = isRight ? &NeumannSolverGSVarEps3D::createPartialNeumannSolver<0, 1, 0>  : rightDirichlet;
    downNeumann  = isDown  ? &NeumannSolverGSVarEps3D::createPartialNeumannSolver<0, 0, -1> : downDirichlet;
    upNeumann    = isUp    ? &NeumannSolverGSVarEps3D::createPartialNeumannSolver<0, 0, 1>  : upDirichlet;
}

inline double NeumannSolverGSVarEps3D::getSumEps(size_t i, size_t j, size_t k) const noexcept
{
    return 6*eps(i, j, k) + eps(i+1, j, k) + eps(i-1, j, k) + eps(i, j+1, k) + eps(i, j-1, k) + eps(i, j, k+1) + eps(i, j, k-1);
}

inline void NeumannSolverGSVarEps3D::solveDirichlet(Matrix3D& u, size_t i, size_t j, size_t k) noexcept
{
    const double epsNI = eps(i+1, j, k) + eps(i, j, k);
    const double epsPI = eps(i-1, j, k) + eps(i, j, k);
    const double epsNJ = eps(i, j+1, k) + eps(i, j, k);
    const double epsPJ = eps(i, j-1, k) + eps(i, j, k);
    const double epsNK = eps(i, j, k+1) + eps(i, j, k);
    const double epsPK = eps(i, j, k-1) + eps(i, j, k);

    u(i, j, k) = (epsPI*u(i-1, j, k) + epsNI*u(i+1, j, k) + epsPJ*u(i, j-1, k) + epsNJ*u(i, j+1, k) +
                  epsPK*u(i, j, k-1) + epsNK*u(i, j, k+1) + 2*h*h*f(i, j, k)) /
                 (epsNI + epsPI + epsNJ + epsPJ + epsNK + epsPK);
}

void NeumannSolverGSVarEps3D::gaussSeidelOneColour(IGaussSeidelSolverStrategy3D::Colour colour) noexcept
{
    generalSolverTemplate(&NeumannSolverGSVarEps3D::solveNeumann<PartialSolver>,
                          &NeumannSolverGSVarEps3D::solveDirichlet, u, colour);
}

void NeumannSolverGSVarEps3D::solve() noexcept
{
    gaussSeidelOneColour(Colour::RED);
    gaussSeidelOneColour(Colour::BLACK);
}

void NeumannSolverGSVarEps3D::findResidualDirichlet(Matrix3D& r, size_t i, size_t j, size_t k) const noexcept
{
    const double epsNI = eps(i+1, j, k) + eps(i, j, k);
    const double epsPI = eps(i-1, j, k) + eps(i, j, k);
    const double epsNJ = eps(i, j+1, k) + eps(i, j, k);
    const double epsPJ = eps(i, j-1, k) + eps(i, j, k);
    const double epsNK = eps(i, j, k+1) + eps(i, j, k);
    const double epsPK = eps(i, j, k-1) + eps(i, j, k);

    r(i, j, k) += (epsPI*u(i-1, j, k) + epsNI*u(i+1, j, k) + epsPJ*u(i, j-1, k) + epsNJ*u(i, j+1, k) +
                   epsPK*u(i, j, k-1) + epsNK*u(i, j, k+1) -
                   (epsNI + epsPI + epsNJ + epsPJ + epsNK + epsPK)*u(i, j, k)) / (2*h*h);
}

void NeumannSolverGSVarEps3D::findResidual(Matrix3D& r) noexcept
{
    generalSolverTemplate(&NeumannSolverGSVarEps3D::findResidualNeumann<PartialSolver>,
                          &NeumannSolverGSVarEps3D::findResidualDirichlet, r, Colour::RED);
    generalSolverTemplate(&NeumannSolverGSVarEps3D::findResidualNeumann<PartialSolver>,
                          &NeumannSolverGSVarEps3D::findResidualDirichlet, r, Colour::BLACK);
}

double NeumannSolverGSVarEps3D::findSumResidual() noexcept
{
    double sum = 0;
    generalSolverTemplate(&NeumannSolverGSVarEps3D::findSumResidualNeumann<PartialSolver>,
                          &NeumannSolverGSVarEps3D::findSumResidualDirichlet, sum, Colour::RED);
    generalSolverTemplate(&NeumannSolverGSVarEps3D::findSumResidualNeumann<PartialSolver>,
                          &NeumannSolverGSVarEps3D::findSumResidualDirichlet, sum, Colour::BLACK);
    return sum;
}

void NeumannSolverGSVarEps3D::findSumResidualDirichlet(double& r, size_t i, size_t j, size_t k) const noexcept
{
    const double epsNI = eps(i+1, j, k) + eps(i, j, k);
    const double epsPI = eps(i-1, j, k) + eps(i, j, k);
    const double epsNJ = eps(i, j+1, k) + eps(i, j, k);
    const double epsPJ = eps(i, j-1, k) + eps(i, j, k);
    const double epsNK = eps(i, j, k+1) + eps(i, j, k);
    const double epsPK = eps(i, j, k-1) + eps(i, j, k);

    r += f(i, j, k) + (epsPI*u(i-1, j, k) + epsNI*u(i+1, j, k) + epsPJ*u(i, j-1, k) + epsNJ*u(i, j+1, k) +
                       epsPK*u(i, j, k-1) + epsNK*u(i, j, k+1) -
                       (epsNI + epsPI + epsNJ + epsPJ + epsNK + epsPK)*u(i, j, k)) / (2*h*h);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
Solver3DPtr
SolverGSStrategy3DFactory::createSolverConstEps(ConditionType3D front, ConditionType3D back, ConditionType3D up,
                                                ConditionType3D down, ConditionType3D left, ConditionType3D right,
                                                Matrix3D& u, const Matrix3D& f, double h, double eps)
{
    if(front == ConditionType3D::DirichletFront and back == ConditionType3D::DirichletBack and
       up == ConditionType3D::DirichletUp and down == ConditionType3D::DirichletDown and
       left == ConditionType3D::DirichletLeft and right == ConditionType3D::DirichletRight)
    {
       return make_unique<AllDirichletGSConstEps3D>(u, f, h, eps);
    }

    const bool frontNeumann = front == ConditionType3D::NeumannFront;
    const bool backNeumann = back == ConditionType3D::NeumannBack;
    const bool upNeumann = up == ConditionType3D::NeumannUp;
    const bool downNeumann = down == ConditionType3D::NeumannDown;
    const bool leftNeumann = left == ConditionType3D::NeumannLeft;
    const bool rightNeumann = right == ConditionType3D::NeumannRight;

    using namespace std;

    if(not frontNeumann and not backNeumann and not upNeumann and not downNeumann and not leftNeumann and not rightNeumann)
    {
        throw std::logic_error("Wrong conditions types passed!");
    }

    return make_unique<NeumannSolverGSConstEps3D>(frontNeumann, backNeumann, upNeumann, downNeumann,
                                                  leftNeumann, rightNeumann, u, f, h, eps);

}

Solver3DPtr
SolverGSStrategy3DFactory::createSolverVarEps(ConditionType3D front, ConditionType3D back, ConditionType3D up,
                                              ConditionType3D down, ConditionType3D left, ConditionType3D right,
                                              Matrix3D& u, const Matrix3D& f, double h, const Matrix3D& eps)
{
    if(front == ConditionType3D::DirichletFront and back == ConditionType3D::DirichletBack and
       up == ConditionType3D::DirichletUp and down == ConditionType3D::DirichletDown and
       left == ConditionType3D::DirichletLeft and right == ConditionType3D::DirichletRight)
    {
        return make_unique<AllDirichletGSVarEps3D>(u, f, h, eps);
    }

    const bool frontNeumann = front == ConditionType3D::NeumannFront;
    const bool backNeumann = back == ConditionType3D::NeumannBack;
    const bool upNeumann = up == ConditionType3D::NeumannUp;
    const bool downNeumann = down == ConditionType3D::NeumannDown;
    const bool leftNeumann = left == ConditionType3D::NeumannLeft;
    const bool rightNeumann = right == ConditionType3D::NeumannRight;

    if(not frontNeumann and not backNeumann and not upNeumann and not downNeumann and not leftNeumann and not rightNeumann)
    {
        throw std::logic_error("Wrong conditions types passed!");
    }

    return make_unique<NeumannSolverGSVarEps3D>(frontNeumann, backNeumann, upNeumann, downNeumann, leftNeumann,
                                                rightNeumann, u, f, h, eps);
}

}

}
