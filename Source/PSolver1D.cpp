#include "PoissonAGH/Solver/PSolver1D.hpp"
#include <numeric>
#include <cmath>

namespace PoissonAGH
{

namespace Solver
{

namespace Detail
{

void PConstEpsInitializer::modifyBoundary(
        Sparse& eqMatrix, Array1D& phi, Boundary1D boundary, GridStep h, const PhysicalConstant& eps,
        ConditionTags1D::DirichletDirichlet)
{
    const auto size = phi.size();
    phi[0] -= boundary.left;
    phi[size - 1] -= boundary.right;
}

void PConstEpsInitializer::modifyBoundary(
        Sparse& eqMatrix, Array1D& phi, Boundary1D boundary, GridStep h, const PhysicalConstant& eps,
        ConditionTags1D::DirichletNeumann)
{
    const auto size = phi.size();
    phi[0] -= boundary.left;
    phi[size - 1] /= 2.;
    phi[size - 1] -= h * boundary.right / eps;
    eqMatrix.D[size- 1] = -1.;
}

void PConstEpsInitializer::modifyBoundary(
        Sparse& eqMatrix, Array1D& phi, Boundary1D boundary, GridStep h, const PhysicalConstant& eps,
        ConditionTags1D::NeumannDirichlet)
{
    const auto size = phi.size();
    phi[0] /= 2.;
    phi[0] -= h * boundary.left / eps;
    phi[size - 1] -= boundary.right;
    eqMatrix.D[0] = -1.;
}

void PConstEpsInitializer::modifyBoundary(
        Sparse& eqMatrix, Array1D& phi, Boundary1D boundary, GridStep h, const PhysicalConstant& eps,
        ConditionTags1D::Periodic)
{
    const auto size = phi.size();
    auto& D = eqMatrix.D;
    auto& DL = eqMatrix.DL;
    auto& DU = eqMatrix.DU;

    D.insert(D.begin(), 1.);
    DL.push_back(1.);
    DU[0] = 0.;
    DU[size-1] = 1.;
    DU.push_back(1.);
    phi.insert(phi.begin(), boundary.left);
}

void PVarEpsInitializer::modifyBoundary(
        Sparse& eqMatrix, Array1D& phi, Boundary1D boundary, GridStep h, const Array1D& epsilonArr,
        ConditionTags1D::DirichletDirichlet)
{
    const auto size = phi.size();
    phi[0] -= boundary.left * 2 * epsilonArr[0];
    phi[size - 1] -= boundary.right * 2 * epsilonArr[size - 1];
}

void PVarEpsInitializer::modifyBoundary(
        Sparse& eqMatrix, Array1D& phi, Boundary1D boundary, GridStep h, const Array1D& epsilonArr,
        ConditionTags1D::DirichletNeumann)
{
    const auto size = phi.size();
    phi[0] -= boundary.left * 2 * epsilonArr[0];
    phi[size - 1] -= 4 * h * boundary.right * epsilonArr[size - 1];
    eqMatrix.DL[size - 1] = 3 * epsilonArr[size - 1] + epsilonArr[size - 2];
}

void PVarEpsInitializer::modifyBoundary(
        Sparse& eqMatrix, Array1D& phi, Boundary1D boundary, GridStep h, const Array1D& epsilonArr,
        ConditionTags1D::NeumannDirichlet)
{
    const auto size = phi.size();
    phi[0] -= 4 * h * boundary.left * epsilonArr[0];
    phi[size - 1] -= boundary.right * 2 * epsilonArr[size - 1];
    eqMatrix.DU[0] = 3 * epsilonArr[0] + epsilonArr[1];
}

void PVarEpsInitializer::modifyBoundary(
        Sparse& eqMatrix, Array1D& phi, Boundary1D boundary, GridStep h, const Array1D& epsilonArr,
        ConditionTags1D::Periodic)
{
    const auto size = phi.size();
    auto& D = eqMatrix.D;
    auto& DL = eqMatrix.DL;
    auto& DU = eqMatrix.DU;

    D.insert(D.begin(), 1);
    DU[size-1] = DL[size-1];
    DL[0] = DU[0];
    DL.insert(DL.begin(), 0.);
    DU.insert(DU.begin(), 0.);
    phi.insert(phi.begin(), boundary.left);
}

void Algorithm1D::thomasAlgorithm(const Sparse& eqMatrix, Array1D& f)
{
    // https://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm

    const auto N = f.size();
    const auto& DL = eqMatrix.DL;
    const auto& D = eqMatrix.D;
    auto DU = eqMatrix.DU;

    DU[0] /= D[0];
    f[0] /= D[0];

    for(auto i = 1u; i < N; ++i)
    {
        auto m = 1. / (D[i] - DL[i] * DU[i-1]);
        DU[i] *= m;
        f[i] = (f[i] - DL[i] * f[i-1]) * m;
    }

    for(auto i = N-1; i-- > 0; )
    {
        f[i] = f[i] - DU[i]*f[i+1];
    }
}

void Algorithm1D::shermanMorrison(Sparse& eqMatrix, Array1D& f)
{
    // http://www.cs.princeton.edu/courses/archive/fall11/cos323/notes/cos323_f11_lecture06_linsys2.pdf
    const auto N = f.size();
    auto& DL = eqMatrix.DL;
    auto& D = eqMatrix.D;
    auto& DU = eqMatrix.DU;

    Array1D u(N, 0.);
    Array1D v(N, 0.);

    u[0] = -D[0];
    u[N-1] = DU[N-1];

    v[0] = 1.;
    v[N-1] = -DL[0]/D[0];

    D[N-1] += DL[0]*DU[N-1] / D[0];
    D[0] += D[0];
    DU[N-1] = DL[0] = 0.;

    thomasAlgorithm(eqMatrix, u);
    auto beta = std::inner_product(begin(v), end(v), begin(u), 0.);
    // if beta is about -1, matrix most probably is singular, so further calculation makes no sense
    if(fabs(1 + beta) < 1e-3)
    {
        throw std::runtime_error("Equation matrix is ill conditioned, calculation stopped.");
    }

    thomasAlgorithm(eqMatrix, f);
    auto alpha = std::inner_product(begin(v), end(v), begin(f), 0.);

    auto scale = alpha / (1 + beta);

    for(auto i = 0u; i < N; ++i)
    {
        f[i] -= u[i] * scale;
    }

}

}

}

}
